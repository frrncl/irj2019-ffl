%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2018a or higher
% * *Copyright:* (C) 2018 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

diary off;

%% Path Configuration

% if we are running on the cluster 
if (strcmpi(computer, 'GLNXA64'))
    addpath('/nas1/promise/ims/ferro/matters/base/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/analysis/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/io/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/measure/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/plot/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/util/')
elseif (strcmpi(computer, 'PCWIN64'))
    addpath('C:\Users\eleon\Desktop\core\core\analysis\')
    addpath('C:\Users\eleon\Desktop\core\core\io\')
    addpath('C:\Users\eleon\Desktop\core\core\measure\')
    addpath('C:\Users\eleon\Desktop\core\core\plot\')
    addpath('C:\Users\eleon\Desktop\core\core\util\')
end

% The base path
if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster    
    EXPERIMENT.path.base = '/nas1/promise/ims/ferro/IRJ2018/experiment/';
elseif(strcmpi(computer, 'PCWIN64'))
    EXPERIMENT.path.base = 'C:\Users\eleon\Dropbox\IRJ2018\experiment\';
else % if we are running on the local machine
    EXPERIMENT.path.base = '/Users/ferro/Documents/pubblicazioni/2018/IRJ2018/experiment/';  
end

% The path for the datasets, i.e. the runs and the pools of both original
% tracks and sub-corpora
EXPERIMENT.path.dataset = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'dataset', filesep);

% The path for the measures
EXPERIMENT.path.measure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'measure', filesep);

% The path for analyses
EXPERIMENT.path.analysis = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'analysis', filesep);

% The path for figures
EXPERIMENT.path.figure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'figure', filesep);

% The path for reports
EXPERIMENT.path.report = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'report', filesep);

%% General Configuration

% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'IRJ 2018';


%% Configuration for Tracks

EXPERIMENT.track.list = {'T08', 'T08_100', 'T08_250', 'T08_500', 'T13', 'T13_500', 'T26', 'T26_100', 'T26_250', 'T26_500', 'T26_10k'};
EXPERIMENT.track.number = length(EXPERIMENT.track.list);

% TREC 08, 1999, Adhoc - Official Length Runs
EXPERIMENT.track.T08.id = 'T08';
EXPERIMENT.track.T08.name = 'TREC 08, 1999, Adhoc';
EXPERIMENT.track.T08.runs = 129;
EXPERIMENT.track.T08.topics = 50;
EXPERIMENT.track.T08.runLength = 1000;
EXPERIMENT.track.T08.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/pool/qrels.trec8.adhoc.txt';
EXPERIMENT.track.T08.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T08.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T08.pool.delimiter = 'space';
EXPERIMENT.track.T08.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T08.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T08.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T08.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T08.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/runs/all';
EXPERIMENT.track.T08.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T08.run.singlePrecision = true;
EXPERIMENT.track.T08.run.delimiter = 'tab';
EXPERIMENT.track.T08.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T08.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T08.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T08.run.delimiter, 'Verbose', false);

% TREC 08, 1999, Adhoc - - Run Length at 100 documents
EXPERIMENT.track.T08_100.id = 'T08_100';
EXPERIMENT.track.T08_100.name = 'TREC 08, 1999, Adhoc';
EXPERIMENT.track.T08_100.runs = 129;
EXPERIMENT.track.T08_100.topics = 50;
EXPERIMENT.track.T08_100.runLength = 100;
EXPERIMENT.track.T08_100.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/pool/qrels.trec8.adhoc.txt';
EXPERIMENT.track.T08_100.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T08_100.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T08_100.pool.delimiter = 'space';
EXPERIMENT.track.T08_100.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T08_100.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T08_100.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T08.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T08_100.run.path = [];
EXPERIMENT.track.T08_100.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T08_100.run.singlePrecision = true;
EXPERIMENT.track.T08_100.run.delimiter = 'tab';
EXPERIMENT.track.T08_100.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T08_100.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T08_100.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T08_100.run.delimiter, 'Verbose', false);

% TREC 08, 1999, Adhoc - - Run Length at 250 documents
EXPERIMENT.track.T08_250.id = 'T08_250';
EXPERIMENT.track.T08_250.name = 'TREC 08, 1999, Adhoc';
EXPERIMENT.track.T08_250.runs = 129;
EXPERIMENT.track.T08_250.topics = 50;
EXPERIMENT.track.T08_250.runLength = 250;
EXPERIMENT.track.T08_250.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/pool/qrels.trec8.adhoc.txt';
EXPERIMENT.track.T08_250.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T08_250.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T08_250.pool.delimiter = 'space';
EXPERIMENT.track.T08_250.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T08_250.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T08_250.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T08.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T08_250.run.path = [];
EXPERIMENT.track.T08_250.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T08_250.run.singlePrecision = true;
EXPERIMENT.track.T08_250.run.delimiter = 'tab';
EXPERIMENT.track.T08_250.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T08_250.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T08_250.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T08_250.run.delimiter, 'Verbose', false);

% TREC 08, 1999, Adhoc - - Run Length at 100 documents
EXPERIMENT.track.T08_500.id = 'T08_500';
EXPERIMENT.track.T08_500.name = 'TREC 08, 1999, Adhoc';
EXPERIMENT.track.T08_500.runs = 129;
EXPERIMENT.track.T08_500.topics = 50;
EXPERIMENT.track.T08_500.runLength = 100;
EXPERIMENT.track.T08_500.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/pool/qrels.trec8.adhoc.txt';
EXPERIMENT.track.T08_500.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T08_500.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T08_500.pool.delimiter = 'space';
EXPERIMENT.track.T08_500.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T08_500.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T08_500.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T08.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T08_500.run.path = [];
EXPERIMENT.track.T08_500.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T08_500.run.singlePrecision = true;
EXPERIMENT.track.T08_500.run.delimiter = 'tab';
EXPERIMENT.track.T08_500.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T08_500.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T08_500.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T08_500.run.delimiter, 'Verbose', false);

% Track TREC 13, 2004, Robust
EXPERIMENT.track.T13.id = 'T13';
EXPERIMENT.track.T13.name =  'TREC 13, 2004, Robust - Official Length Runs';
EXPERIMENT.track.T13.runs = 110;
EXPERIMENT.track.T13.topics = 249;
EXPERIMENT.track.T13.runLength = 1000;
EXPERIMENT.track.T13.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_13_2004_Robust/pool/qrels.robust2004.txt';
EXPERIMENT.track.T13.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T13.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T13.pool.delimiter = 'space';
EXPERIMENT.track.T13.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T13.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T13.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T13.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T13.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_13_2004_Robust/runs';
EXPERIMENT.track.T13.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T13.run.singlePrecision = true;
EXPERIMENT.track.T13.run.delimiter = 'tab';
EXPERIMENT.track.T13.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T13.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T13.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T13.run.delimiter, 'Verbose', false);


% Track TREC 13, 2004, Robust - Run Length at 500 documents
EXPERIMENT.track.T13_500.id = 'T13_500';
EXPERIMENT.track.T13_500.name =  'TREC 13, 2004, Robust - Run Length at 500 documents';
EXPERIMENT.track.T13_500.runs = 110;
EXPERIMENT.track.T13_500.topics = 249;
EXPERIMENT.track.T13_500.runLength = 500;
EXPERIMENT.track.T13_500.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_13_2004_Robust/pool/qrels.robust2004.txt';
EXPERIMENT.track.T13_500.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T13_500.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T13_500.pool.delimiter = 'space';
EXPERIMENT.track.T13_500.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T13_500.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T13_500.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T13_500.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T13_500.run.path = [];
EXPERIMENT.track.T13_500.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T13_500.run.singlePrecision = true;
EXPERIMENT.track.T13_500.run.delimiter = 'tab';
EXPERIMENT.track.T13_500.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T13_500.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T13_500.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T13_500.run.delimiter, 'Verbose', false);


% Track TREC 26, 2017, Core - Official Length Runs
EXPERIMENT.track.T26_10k.id = 'T26_10k';
EXPERIMENT.track.T26_10k.name =  'TREC 26, 2017, Core - Official Length Runs';
EXPERIMENT.track.T26_10k.runs = 75;
EXPERIMENT.track.T26_10k.topics = 50;
EXPERIMENT.track.T26_10k.runLength = 10000;
EXPERIMENT.track.T26_10k.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/pool/core-50topics-nist-qrels.txt';
EXPERIMENT.track.T26_10k.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T26_10k.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T26_10k.pool.delimiter = 'space';
EXPERIMENT.track.T26_10k.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T26_10k.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T26_10k.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T26_10k.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T26_10k.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/runs/all';
EXPERIMENT.track.T26_10k.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T26_10k.run.singlePrecision = true;
EXPERIMENT.track.T26_10k.run.delimiter = 'tab';
EXPERIMENT.track.T26_10k.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T26_10k.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T26_10k.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T26_10k.run.delimiter, 'Verbose', false);

% Track TREC 26, 2017, Core
EXPERIMENT.track.T26.id = 'T26';
EXPERIMENT.track.T26.name =  'TREC 26, 2017, Core - Run Length at 1000 documents';
EXPERIMENT.track.T26.runs = 75;
EXPERIMENT.track.T26.topics = 50;
EXPERIMENT.track.T26.runLength = 1000;
EXPERIMENT.track.T26.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/pool/core-50topics-nist-qrels.txt';
EXPERIMENT.track.T26.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T26.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T26.pool.delimiter = 'space';
EXPERIMENT.track.T26.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T26.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T26.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T26.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T26.run.path = [];
EXPERIMENT.track.T26.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T26.run.singlePrecision = true;
EXPERIMENT.track.T26.run.delimiter = 'tab';
EXPERIMENT.track.T26.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T26.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T26.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T26.run.delimiter, 'Verbose', false);

% Track TREC 26, 2017, Core - Run Length at 100 documents
EXPERIMENT.track.T26_100.id = 'T26_100';
EXPERIMENT.track.T26_100.name =  'TREC 26, 2017, Core';
EXPERIMENT.track.T26_100.runs = 75;
EXPERIMENT.track.T26_100.topics = 50;
EXPERIMENT.track.T26_100.runLength = 100;
EXPERIMENT.track.T26_100.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/pool/core-50topics-nist-qrels.txt';
EXPERIMENT.track.T26_100.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T26_100.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T26_100.pool.delimiter = 'space';
EXPERIMENT.track.T26_100.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T26_100.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T26_100.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T26_100.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T26_100.run.path = [];
EXPERIMENT.track.T26_100.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T26_100.run.singlePrecision = true;
EXPERIMENT.track.T26_100.run.delimiter = 'tab';
EXPERIMENT.track.T26_100.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T26_100.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T26_100.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T26_100.run.delimiter, 'Verbose', false);


% Track TREC 26, 2017, Core - Run Length at 250 documents
EXPERIMENT.track.T26_250.id = 'T26_250';
EXPERIMENT.track.T26_250.name =  'TREC 26, 2017, Core';
EXPERIMENT.track.T26_250.runs = 75;
EXPERIMENT.track.T26_250.topics = 50;
EXPERIMENT.track.T26_250.runLength = 250;
EXPERIMENT.track.T26_250.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/pool/core-50topics-nist-qrels.txt';
EXPERIMENT.track.T26_250.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T26_250.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T26_250.pool.delimiter = 'space';
EXPERIMENT.track.T26_250.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T26_250.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T26_250.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T26_250.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T26_250.run.path = [];
EXPERIMENT.track.T26_250.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T26_250.run.singlePrecision = true;
EXPERIMENT.track.T26_250.run.delimiter = 'tab';
EXPERIMENT.track.T26_250.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T26_250.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T26_250.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T26_250.run.delimiter, 'Verbose', false);

% Track TREC 26, 2017, Core - Run Length at 250 documents
EXPERIMENT.track.T26_500.id = 'T26_500';
EXPERIMENT.track.T26_500.name =  'TREC 26, 2017, Core';
EXPERIMENT.track.T26_500.runs = 75;
EXPERIMENT.track.T26_500.topics = 50;
EXPERIMENT.track.T26_500.runLength = 500;
EXPERIMENT.track.T26_500.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_26_2017_Core/pool/core-50topics-nist-qrels.txt';
EXPERIMENT.track.T26_500.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T26_500.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T26_500.pool.delimiter = 'space';
EXPERIMENT.track.T26_500.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T26_500.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T26_500.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T26_500.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T26_500.run.path = [];
EXPERIMENT.track.T26_500.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T26_500.run.singlePrecision = true;
EXPERIMENT.track.T26_500.run.delimiter = 'tab';
EXPERIMENT.track.T26_500.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T26_500.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T26_500.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T26_500.run.delimiter, 'Verbose', false);

% Returns the name of a track given its index in EXPERIMENT.track.list
EXPERIMENT.track.getName = @(idx) ( EXPERIMENT.track.(EXPERIMENT.track.list{idx}).name ); 

%% Patterns for file names

% MAT - Pattern <path>/<trackID>/<fileID>.<ext> 
EXPERIMENT.pattern.file.general = @(path, trackID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s.%5$s', path, trackID, filesep, fileID, ext);
 
% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/<datasetID>.mat
EXPERIMENT.pattern.file.dataset = @(trackID, datasetID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.dataset, trackID, datasetID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<measureID>.mat
EXPERIMENT.pattern.file.measure = @(trackID, measureID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.measure, trackID, measureID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/analysis/<trackID>/<analysisID>.mat
EXPERIMENT.pattern.file.analysis = @(trackID, analysisID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.analysis, trackID, analysisID, 'mat');

% PDF - Pattern EXPERIMENT.path.base/figure/<trackID>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, figureID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.figure, trackID, figureID, 'pdf');

% TEX - Pattern EXPERIMENT.path.base/report/<trackID>/<reportID>
EXPERIMENT.pattern.file.report = @(trackID, reportID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.report, trackID, reportID, 'tex');


%% Patterns for identifiers

% Pattern pool_<trackID>
EXPERIMENT.pattern.identifier.pool =  @(trackID) sprintf('pool_%1$s', trackID);

% Pattern <downsampling>_pool_<trackID>
EXPERIMENT.pattern.identifier.downsampledPool =  @(downsampling, trackID) sprintf('%1$s_pool_%2$s', downsampling, trackID);

% Pattern run_<trackID>
EXPERIMENT.pattern.identifier.run = @(trackID) sprintf('run_%1$s', trackID);

% Pattern _<measureID>_<trackID>
EXPERIMENT.pattern.identifier.measure = @(measureID, trackID) sprintf('%1$s_%2$s', measureID, trackID);

% Pattern <downsampling>_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.downsampledMeasure = @(downsampling, measureID, trackID) sprintf('%1$s_%2$s_%3$s', downsampling, measureID, trackID);

% Pattern <correlationID>_<trackID>
EXPERIMENT.pattern.identifier.correlation = @(correlationID, trackID) sprintf('%1$s_%2$s', correlationID, trackID);

% Pattern selfK_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.downsampledSelfKendal = @(downsampling, measureID, trackID) sprintf('selfK_%1$s_%2$s_%3$s', downsampling, measureID, trackID);

% Pattern dpow_<downsampling>_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.downsampledDiscPower = @(downsampling, measureID, trackID) sprintf('dpow_%1$s_%2$s_%3$s', downsampling, measureID, trackID);

% Pattern dpow_replicates_<trackID>
EXPERIMENT.pattern.identifier.discPowReplicates = @(trackID) sprintf('dpow_replicates_%1$s', trackID);

% Pattern <type>_<downsampling>_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.discPow.general = @(type, downsampling, measureID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s', type, downsampling, measureID, trackID);

% Pattern asl_<downsampling>_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.discPow.asl = @(downsampling, measureID, trackID) EXPERIMENT.pattern.identifier.discPow.general('asl', downsampling, measureID, trackID);

% Pattern dp_<downsampling>_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.discPow.dp = @(downsampling, measureID, trackID) EXPERIMENT.pattern.identifier.discPow.general('dp', downsampling, measureID, trackID);

% Pattern delta_<downsampling>_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.discPow.delta = @(downsampling, measureID, trackID) EXPERIMENT.pattern.identifier.discPow.general('delta', downsampling, measureID, trackID);

% Pattern <rqID>_<type>_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.general =  @(rqID, type, measureID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s', rqID, type, measureID, trackID);

% Pattern <rqID>_anova_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.analysis =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'anova', measureID, trackID);

% Pattern <rqID>_obs_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.obs =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'obs', measureID, trackID);

% Pattern <rqID>_tbl_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.tbl =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'tbl', measureID, trackID);

% Pattern <rqID>_sts_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.sts =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'sts', measureID, trackID);

% Pattern <rqID>_me_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.me =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'me', measureID, trackID);

% Pattern <rqID>_ie_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.ie =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'ie', measureID, trackID);

% Pattern <rqID>_mie_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mie =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'mie', measureID, trackID);

% Pattern <rqID>_mc_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mc =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'mc', measureID, trackID);

% Pattern <rqID>_soa_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.soa =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'soa', measureID, trackID);

% Pattern <rqID>_<factor>-me_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mePlot =  @(rqID, factor, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, [factor '-me'], measureID, trackID);

% Pattern <rqID>_<factor1>-<factor2>-ie_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.iePlot =  @(rqID, factor1, factor2, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, [factor1 '-' factor2 '-ie'], measureID, trackID);


% Pattern <rqID>_anova_<trackID>
EXPERIMENT.pattern.identifier.rep.anova =  @(rqID, trackID) sprintf('%1$s_%2$s_%3$s', rqID, 'anova', trackID);

% Pattern <rqID>_anova_<trackID>
EXPERIMENT.pattern.identifier.rep.summaryAnova =  @(rqID, trackID) sprintf('%1$s_%2$s_%3$s', rqID, 'summary_anova', trackID);


% Pattern <rqID>_tukey_<trackID>
EXPERIMENT.pattern.identifier.rep.tukey =  @(rqID, trackID) sprintf('%1$s_%2$s_%3$s', rqID, 'tukey', trackID);



%% Configuration for measures

% The list of measures under experimentation
% EXPERIMENT.measure.list = {'ap', 'precision', 'recall', 'fMeasure', ...                          
%                            'dcg', 'ndcg', 'err', ...
%                            'rbp05', 'rbp08', 'grbp03', 'grbp08', ...
%                            'sbto', 'rbto', ...
%                            'gR', 'gP', ...
%                            'rbp02', 'grbp02', ...
%                            'gRni', 'gPni', 'grbp03ni', ...
%                            'rbtoW2', 'rbtoW3', ...
%                            'dcgW2', 'errW2'};
                       
% EXPERIMENT.measure.list = {'precision', 'recall', 'fMeasure', ...                          
%                            'gR', 'gP', ...
%                            'ap', 'dcg', 'ndcg', 'err', ...
%                            'rbp02', 'rbp05', 'rbp08', ...
%                            'grbp02', 'grbp03', 'grbp08', ...
%                            'sbto', ...
%                            'rbto'};                       
                            
EXPERIMENT.measure.list = {'ap', 'precision', 'recall', 'fMeasure', ...                          
                            'dcg', 'ndcg', 'err', ...
                            'rbp05', 'rbp08', 'grbp03', 'grbp08', ...
                            'sbto', 'rbto', ...
                            'gR', 'gP', ...
                            'rbp02', 'grbp02'};
                       
                       
EXPERIMENT.measure.number = length(EXPERIMENT.measure.list);

% Configuration for Precision
EXPERIMENT.measure.precision.id = 'precision';
EXPERIMENT.measure.precision.acronym = 'P';
EXPERIMENT.measure.precision.name = 'Precision';
EXPERIMENT.measure.precision.compute = @(pool, runSet, shortNameSuffix, runLength) setBasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'FixNumberRetrievedDocuments', runLength);

% Configuration for Recall
EXPERIMENT.measure.recall.id = 'recall';
EXPERIMENT.measure.recall.acronym = 'R';
EXPERIMENT.measure.recall.name = 'Recall';
EXPERIMENT.measure.recall.compute = @(pool, runSet, shortNameSuffix, runLength) recall(pool, runSet, 'ShortNameSuffix', shortNameSuffix);

% Configuration for F-measure
EXPERIMENT.measure.fMeasure.id = 'fMeasure';
EXPERIMENT.measure.fMeasure.acronym = 'F';
EXPERIMENT.measure.fMeasure.name = 'F-measure';
EXPERIMENT.measure.fMeasure.compute = @(pool, runSet, shortNameSuffix, runLength) fMeasure(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'FixNumberRetrievedDocuments', runLength);

% Configuration for Generalized Recall
EXPERIMENT.measure.gR.id = 'gR';
EXPERIMENT.measure.gR.acronym = 'gR';
EXPERIMENT.measure.gR.name = 'Generalized Recall';
EXPERIMENT.measure.gR.compute = @(pool, runSet, shortNameSuffix, runLength) generalizedRecall(pool, runSet, 'ShortNameSuffix', shortNameSuffix);

% Configuration for Generalized Recall - Not Interval Scale
EXPERIMENT.measure.gRni.id = 'gRni';
EXPERIMENT.measure.gRni.acronym = 'gR_ni';
EXPERIMENT.measure.gRni.name = 'Generalized Recall - Not Interval Scale';
EXPERIMENT.measure.gRni.relevanceWeights = [0 1 3];
EXPERIMENT.measure.gRni.compute = @(pool, runSet, shortNameSuffix, runLength) generalizedRecall(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'MapToRelevanceWeights', EXPERIMENT.measure.gRni.relevanceWeights);

% Configuration for Generalized Precision
EXPERIMENT.measure.gP.id = 'gP';
EXPERIMENT.measure.gP.acronym = 'gP';
EXPERIMENT.measure.gP.name = 'Generalized Precision';
EXPERIMENT.measure.gP.compute = @(pool, runSet, shortNameSuffix, runLength) generalizedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'FixNumberRetrievedDocuments', runLength);

% Configuration for Generalized Precision - Not Interval Scale
EXPERIMENT.measure.gPni.id = 'gPni';
EXPERIMENT.measure.gPni.acronym = 'gP_ni';
EXPERIMENT.measure.gPni.name = 'Generalized Precision - Not Interval Scale';
EXPERIMENT.measure.gPni.relevanceWeights = [0 1 3];
EXPERIMENT.measure.gPni.compute = @(pool, runSet, shortNameSuffix, runLength) generalizedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'FixNumberRetrievedDocuments', runLength, 'MapToRelevanceWeights', EXPERIMENT.measure.gPni.relevanceWeights);

% Configuration for AP
EXPERIMENT.measure.ap.id = 'ap';
EXPERIMENT.measure.ap.acronym = 'AP';
EXPERIMENT.measure.ap.name = 'Average Precision';
EXPERIMENT.measure.ap.compute = @(pool, runSet, shortNameSuffix, runLength) averagePrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix);

% Configuration for DCG
EXPERIMENT.measure.dcg.id = 'dcg';
EXPERIMENT.measure.dcg.acronym = 'DCG';
EXPERIMENT.measure.dcg.name = 'Discounted Cumulated Gain';
EXPERIMENT.measure.dcg.logBase = 10;
EXPERIMENT.measure.dcg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.dcg.compute = @(pool, runSet, shortNameSuffix, runLength) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix,  'CutOffs', runLength, 'LogBase', EXPERIMENT.measure.dcg.logBase, 'Normalize', false, 'FixNumberRetrievedDocuments', runLength, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.dcg.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for DCG
EXPERIMENT.measure.dcgW2.id = 'dcgW2';
EXPERIMENT.measure.dcgW2.acronym = 'DCG';
EXPERIMENT.measure.dcgW2.name = 'Discounted Cumulated Gain';
EXPERIMENT.measure.dcgW2.logBase = 10;
EXPERIMENT.measure.dcgW2.relevanceWeights = [0 1 2];
EXPERIMENT.measure.dcgW2.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.dcgW2.compute = @(pool, runSet, shortNameSuffix, runLength) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix,  'CutOffs', runLength, 'LogBase', EXPERIMENT.measure.dcgW2.logBase, 'Normalize', false, 'FixNumberRetrievedDocuments', runLength, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.dcgW2.fixedNumberRetrievedDocumentsPaddingStrategy, 'MapToRelevanceWeights', EXPERIMENT.measure.dcgW2.relevanceWeights);


% Configuration for nDCG
EXPERIMENT.measure.ndcg.id = 'ndcg';
EXPERIMENT.measure.ndcg.acronym = 'nDCG';
EXPERIMENT.measure.ndcg.name = 'Normalized Discounted Cumulated Gain';
EXPERIMENT.measure.ndcg.logBase = 10;
EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg.compute = @(pool, runSet, shortNameSuffix, runLength) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', runLength, 'LogBase', EXPERIMENT.measure.ndcg.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', runLength, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for ERR
EXPERIMENT.measure.err.id = 'err';
EXPERIMENT.measure.err.acronym = 'ERR';
EXPERIMENT.measure.err.name = 'Expected Reciprocal Rank';
EXPERIMENT.measure.err.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.err.compute = @(pool, runSet, shortNameSuffix, runLength) expectedReciprocalRank(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', runLength, 'FixNumberRetrievedDocuments', runLength, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.err.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for ERR
EXPERIMENT.measure.errW2.id = 'errW2';
EXPERIMENT.measure.errW2.acronym = 'ERR';
EXPERIMENT.measure.errW2.name = 'Expected Reciprocal Rank';
EXPERIMENT.measure.errW2.relevanceWeights = [0 1 2];
EXPERIMENT.measure.errW2.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.errW2.compute = @(pool, runSet, shortNameSuffix, runLength) expectedReciprocalRank(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', runLength, 'FixNumberRetrievedDocuments', runLength, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.errW2.fixedNumberRetrievedDocumentsPaddingStrategy, 'MapToRelevanceWeights', EXPERIMENT.measure.errW2.relevanceWeights);


% Configuration for RBP with p = 0.2
EXPERIMENT.measure.rbp02.id = 'rbp02';
EXPERIMENT.measure.rbp02.acronym = 'RBP_p02';
EXPERIMENT.measure.rbp02.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp02.persistence = 0.2;
EXPERIMENT.measure.rbp02.compute = @(pool, runSet, shortNameSuffix, runLength) rankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.rbp02.persistence);

% Configuration for RBP with p = 0.5
EXPERIMENT.measure.rbp05.id = 'rbp05';
EXPERIMENT.measure.rbp05.acronym = 'RBP_p05';
EXPERIMENT.measure.rbp05.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp05.persistence = 0.5;
EXPERIMENT.measure.rbp05.compute = @(pool, runSet, shortNameSuffix, runLength) rankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.rbp05.persistence);

% Configuration for RBP with p = 0.8
EXPERIMENT.measure.rbp08.id = 'rbp08';
EXPERIMENT.measure.rbp08.acronym = 'RBP_p08';
EXPERIMENT.measure.rbp08.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp08.persistence = 0.8;
EXPERIMENT.measure.rbp08.compute = @(pool, runSet, shortNameSuffix, runLength) rankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.rbp08.persistence);

% Configuration for gRBP with p = 0.2
EXPERIMENT.measure.grbp02.id = 'grbp02';
EXPERIMENT.measure.grbp02.acronym = 'gRBP_p02';
EXPERIMENT.measure.grbp02.name = 'Graded Rank-biased Precision';
EXPERIMENT.measure.grbp02.persistence = 0.2;
EXPERIMENT.measure.grbp02.compute = @(pool, runSet, shortNameSuffix, runLength) gradedRankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.grbp02.persistence);

% Configuration for gRBP with p = 0.3
EXPERIMENT.measure.grbp03.id = 'grbp03';
EXPERIMENT.measure.grbp03.acronym = 'gRBP_p03';
EXPERIMENT.measure.grbp03.name = 'Graded Rank-biased Precision';
EXPERIMENT.measure.grbp03.persistence = 1/3;
EXPERIMENT.measure.grbp03.compute = @(pool, runSet, shortNameSuffix, runLength) gradedRankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.grbp03.persistence);

% Configuration for gRBP with p = 0.3  - Not Interval Scale
EXPERIMENT.measure.grbp03ni.id = 'grbp03ni';
EXPERIMENT.measure.grbp03ni.acronym = 'gRBP_p03_ni';
EXPERIMENT.measure.grbp03ni.name = 'Graded Rank-biased Precision - Not Interval Scale';
EXPERIMENT.measure.grbp03ni.persistence = 1/3;
EXPERIMENT.measure.grbp03ni.relevanceWeights = [0 1 3];
EXPERIMENT.measure.grbp03ni.compute = @(pool, runSet, shortNameSuffix, runLength) gradedRankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.grbp03ni.persistence, 'MapToRelevanceWeights', EXPERIMENT.measure.grbp03ni.relevanceWeights);


% Configuration for gRBP with p = 0.8
EXPERIMENT.measure.grbp08.id = 'grbp08';
EXPERIMENT.measure.grbp08.acronym = 'gRBP_p08';
EXPERIMENT.measure.grbp08.name = 'Graded Rank-biased Precision';
EXPERIMENT.measure.grbp08.persistence = 0.8;
EXPERIMENT.measure.grbp08.compute = @(pool, runSet, shortNameSuffix, runLength) gradedRankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.grbp08.persistence);

% Configuration for SBTO
EXPERIMENT.measure.sbto.id = 'sbto';
EXPERIMENT.measure.sbto.acronym = 'SBTO';
EXPERIMENT.measure.sbto.name = 'Set-Based Total Order';
EXPERIMENT.measure.sbto.compute = @(pool, runSet, shortNameSuffix, runLength) setBasedTotalOrder(pool, runSet, 'ShortNameSuffix', shortNameSuffix);

% Configuration for RBTO
EXPERIMENT.measure.rbto.id = 'rbto';
EXPERIMENT.measure.rbto.acronym = 'RBTO';
EXPERIMENT.measure.rbto.name = 'Rank-Based Total Order';
EXPERIMENT.measure.rbto.compute = @(pool, runSet, shortNameSuffix, runLength) rankBasedTotalOrder(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'FixNumberRetrievedDocuments', runLength);

% Configuration for RBTO - Alternative Relevance Weights 2
EXPERIMENT.measure.rbtoW2.id = 'rbtoW2';
EXPERIMENT.measure.rbtoW2.acronym = 'RBTO_W2';
EXPERIMENT.measure.rbtoW2.relevanceWeights = [0 2 4];
EXPERIMENT.measure.rbtoW2.name = 'Set-Based Total Order - Alternative Relevance Weights 2';
EXPERIMENT.measure.rbtoW2.compute = @(pool, runSet, shortNameSuffix, runLength) rankBasedTotalOrder(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'FixNumberRetrievedDocuments', runLength, 'MapToRelevanceWeights', EXPERIMENT.measure.rbtoW2.relevanceWeights);

% Configuration for RBTO - Alternative Relevance Weights 3
EXPERIMENT.measure.rbtoW3.id = 'rbtoW3';
EXPERIMENT.measure.rbtoW3.acronym = 'RBTO_W3';
EXPERIMENT.measure.rbtoW3.name = 'Rank-Based Total Order - Alternative Relevance Weights 3';
EXPERIMENT.measure.rbtoW3.relevanceWeights = [0 1 3];
EXPERIMENT.measure.rbtoW3.compute = @(pool, runSet, shortNameSuffix, runLength) rankBasedTotalOrder(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'FixNumberRetrievedDocuments', runLength, 'MapToRelevanceWeights', EXPERIMENT.measure.rbtoW3.relevanceWeights);

% Returns the identifier of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getID = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).id ); 

% Returns the acronym of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getAcronym = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).acronym ); 

% Returns the name of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getName = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).name ); 


%% Configuration for analyses

% The significance level for the analyses
EXPERIMENT.analysis.alpha.threshold = 0.05;
EXPERIMENT.analysis.alpha.color = 'lightgrey';

% The confidence degree for computing the confidence interval
EXPERIMENT.analysis.ciAlpha.threshold = 0.05;


% Labels of the correlation pairs
EXPERIMENT.analysis.correlation.labels.list = EXPERIMENT.measure.list(combnk(1:EXPERIMENT.measure.number, 2));
EXPERIMENT.analysis.correlation.labels.first = EXPERIMENT.analysis.correlation.labels.list(:, 1);
EXPERIMENT.analysis.correlation.labels.second = EXPERIMENT.analysis.correlation.labels.list(:, 2);
EXPERIMENT.analysis.correlation.labels.list(:, 3) = EXPERIMENT.analysis.correlation.labels.list(:, 2);
EXPERIMENT.analysis.correlation.labels.list(:, 2) = {'_'};
EXPERIMENT.analysis.correlation.labels.list = strcat(EXPERIMENT.analysis.correlation.labels.list(:, 1), EXPERIMENT.analysis.correlation.labels.list(:, 2), EXPERIMENT.analysis.correlation.labels.list(:, 3));
EXPERIMENT.analysis.correlation.labels.number = length(EXPERIMENT.analysis.correlation.labels.list);

% Configuration for Kendall's tau correlation
EXPERIMENT.analysis.correlation.id = 'tau';
EXPERIMENT.analysis.correlation.latex.symbol = '$\tau$';
EXPERIMENT.analysis.correlation.latex.label = 'Kendall''s $\tau$ Correlation';
EXPERIMENT.analysis.correlation.name = 'Kendall''s tau correlation';
EXPERIMENT.analysis.correlation.compute = @(data) corr(data, 'type', 'Kendall');

% Configuration for pool downsampling
EXPERIMENT.analysis.poolDownsample.downsampling.list = {'srs', 'rs'};
EXPERIMENT.analysis.poolDownsample.downsampling.srs = 'StratifiedRandomSampling';
EXPERIMENT.analysis.poolDownsample.downsampling.rs = 'RandomSampling';
EXPERIMENT.analysis.poolDownsample.reductionRates = [90 70 50 30 10 5];
EXPERIMENT.analysis.poolDownsample.iterations = 1;
EXPERIMENT.analysis.poolDownsample.compute = @(pool, downsampling)  downsamplePool(pool, [], ...
            'Downsampling', EXPERIMENT.analysis.poolDownsample.downsampling.(downsampling), ...
            'SampleSize', EXPERIMENT.analysis.poolDownsample.reductionRates, ...
            'Iterations', EXPERIMENT.analysis.poolDownsample.iterations);
                

% Configuration for discriminative power  
EXPERIMENT.analysis.dpow.method = 'PairedBootstrapTest';
EXPERIMENT.analysis.dpow.samples = 1000;
EXPERIMENT.analysis.dpow.compute.noReplicates = @(input) discriminativePower(input{:}, ...
            'Method', EXPERIMENT.analysis.dpow.method, ...
            'Alpha', EXPERIMENT.analysis.alpha.threshold, ...
            'Samples', EXPERIMENT.analysis.dpow.samples); 
EXPERIMENT.analysis.dpow.compute.replicates = @(input, replicates) discriminativePower(input{:}, ...
            'Replicates', replicates,...
            'Method', EXPERIMENT.analysis.dpow.method, ...
            'Alpha', EXPERIMENT.analysis.alpha.threshold, ...
            'Samples', EXPERIMENT.analysis.dpow.samples); 

        
% Configuration for ANOVA
EXPERIMENT.analysis.anova.smallEffect.threshold = 0.06;
EXPERIMENT.analysis.anova.smallEffect.color = 'verylightblue';

EXPERIMENT.analysis.anova.mediumEffect.threshold = 0.14;
EXPERIMENT.analysis.anova.mediumEffect.color = 'lightblue';

EXPERIMENT.analysis.anova.largeEffect.color = 'blue';


% Labels
EXPERIMENT.analysis.anova.label.subject = 'Topic';
EXPERIMENT.analysis.anova.label.factorA = 'System';

% Colors
EXPERIMENT.analysis.anova.color.subject = rgb('FireBrick');
EXPERIMENT.analysis.anova.color.factorA = rgb('RoyalBlue');

% The Sum of Squares Type to be used
EXPERIMENT.analysis.anova.sstype = 3;

% The list of the possible analyses
EXPERIMENT.analysis.list = {'rq0', 'rq1'};

% Topic/System Effects ANOVA
EXPERIMENT.analysis.anova.rq0.id = 'rq0';
EXPERIMENT.analysis.anova.rq0.name = 'Topic/System Effects';
EXPERIMENT.analysis.anova.rq0.description = 'Crossed effects GLMM: subjects are topics; effects are systems';
EXPERIMENT.analysis.anova.rq0.glmm = '$Y_{ij} = \mu_{\cdot\cdot} + \tau_i + \alpha_j + \varepsilon_{ij}$';
% the model = Topic (subject) + System (factorA)
EXPERIMENT.analysis.anova.rq0.model = [1 0                                       0 1];
EXPERIMENT.analysis.anova.rq0.compute = @(data, subject, factorA) anovan(data, {subject, factorA}, ...
        'Model', EXPERIMENT.analysis.anova.rq0.model, ...
        'VarNames', {EXPERIMENT.analysis.anova.label.subject, EXPERIMENT.analysis.anova.label.factorA}, ...
        'sstype', EXPERIMENT.analysis.anova.sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    

% Topic/System Effects Kruskal-Wallis
EXPERIMENT.analysis.kw.rq1.id = 'rq1';
EXPERIMENT.analysis.kw.rq1.name = 'Topic/System Effects Kruskal-Wallis Test';
EXPERIMENT.analysis.kw.rq1.description = 'Topic/System Effects Kruskal-Wallis Test';
EXPERIMENT.analysis.kw.rq1.compute = @(data) kruskalwallis(data, [], 'off');
                
% Tukey HSD multiple comparison analyses
EXPERIMENT.analysis.multcompare.system = @(sts) multcompare(sts, 'CType', 'hsd', 'Alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [2], 'Display', 'off');

    
    