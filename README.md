Matlab source code for running the experiments reported in the paper:

* Ferrante, M., Ferro, N., and Losiouk, E. (2018). How do interval scales help us to better understand IR evaluation measures?. Submitted to _Information Retrieval Journal_.
