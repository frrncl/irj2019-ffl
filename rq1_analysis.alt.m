%% rq1_analysis
% 
% Computes the Topic/System ANOVA.
%% Synopsis
%
%   [] = rq1_analysis(trackID, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing.

%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2018a or higher
% * *Copyright:* (C) 2018 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq1_analysis(trackID, startMeasure, endMeasure)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq1';
    end

    % check the number of input parameters
    narginchk(1, 3);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    if nargin == 3
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end

    % start of overall computations
    startComputation = tic;
                  
    fprintf('\n\n######## Performing %s analysis on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    
                
    % for each measure
    for m = startMeasure:endMeasure
        
        start = tic;
        
        fprintf('\n+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        fprintf('  - loading the data\n');
        
        mid = EXPERIMENT.measure.list{m};
        
        measureID = EXPERIMENT.pattern.identifier.measure(mid, trackID);
        
        serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
            'WorkspaceVarNames', {'measure'}, ...
            'FileVarNames', {measureID});
        
        fprintf('  - analysing the data\n');   

        % the number of topics and runs
        T = height(measure);
        R = width(measure);

        
        pairs = nchoosek(1:R, 2);
        
        data.pairs = size(pairs, 1);
        
        data.(mid).t.count = 0;
        data.(mid).sign.count = 0;
        data.(mid).signedRank.count = 0;
        data.(mid).rankSum.count = 0;
        
        for i = 1:length(pairs)
            
            [~, p] = ttest(measure{:, pairs(i, 1)}, measure{:, pairs(i, 2)});
            
            if p < EXPERIMENT.analysis.alpha.threshold
              data.(mid).t.count = data.(mid).t.count + 1;  
            end
            
            [p] = signtest(measure{:, pairs(i, 1)}, measure{:, pairs(i, 2)});
            
            if p < EXPERIMENT.analysis.alpha.threshold
              data.(mid).sign.count = data.(mid).sign.count + 1;  
            end
            
            [p] = signrank(measure{:, pairs(i, 1)}, measure{:, pairs(i, 2)});
            
            if p < EXPERIMENT.analysis.alpha.threshold
              data.(mid).signedRank.count = data.(mid).signedRank.count + 1;  
            end
            
            [p] = ranksum(measure{:, pairs(i, 1)}, measure{:, pairs(i, 2)});
            
            if p < EXPERIMENT.analysis.alpha.threshold
              data.(mid).rankSum.count = data.(mid).rankSum.count + 1;  
            end
            
        end
        
         data.(mid).t.ratio = data.(mid).t.count / data.pairs;
        data.(mid).sign.ratio = data.(mid).sign.count / data.pairs;
        data.(mid).signedRank.ration = data.(mid).signedRank.count / data.pairs;
        data.(mid).rankSum.ratio = data.(mid).rankSum.count / data.pairs;
                
        clear measure p;
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end % for measure
  
    disp('Wait');
    
    fprintf('\n\n######## Total elapsed time for performing %s ANOVA analysis on track %s (%s): %s ########\n\n', ...
           TAG, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end

