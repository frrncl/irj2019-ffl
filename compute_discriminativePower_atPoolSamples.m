%% compute_discriminativePower_atPoolSamples
% 
% Load the measures computed among the downsampled pools. For each iteration and pool sample, 
% it computes the discriminative power for every measure and saves it to a |.mat| file.
%
%% Synopsis
%
%   [asl, dp, delta, replicates] = compute_discriminativePower_atPoolSamples(trackID, downsampling, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|downsampling|* - the type of downsampling to be applied.
% * *|startMeasure|* - the index of the start measure to process. Optional.
% * *|endMeasure|* - the index of the end measure to process. Optional.
%
% *Returns*
%
% Nothing
%
%
%% Variables to be saved
%
% * *|asl|*  - is a table containing a row for each iteration and a 
% column for each pool sample. The value of each cell is the ASL
% computed on the given pool sample at the given iteration.
% * *|dp|*  - is a table containing a row for each iteration and a 
% column for each pool sample. The value of each cell is the discriminative
% power computed on the given pool sample at the given iteration.
% * *|delta|*  - is a table containing a row for each iteration and a 
% column for each pool sample. The value of each cell is the delta
% computed on the given pool sample at the given iteration.
% * |replicates| - is an integer-valued matrix containing the
% replicates that have been used for the bootstrap or randomization. In the case of
% the |PairedBootstrapTest|, it is a |t*samples| matrix (|t| number of
% topics) where each column (a replicate) represents a sampling with 
% repetitions of the topics. In the case of the |RandomisedTukeyHSDTest|,
% it is a |samples*(t*s)| matrix (|t| number of topics, |s| number of 
% systems) where each row (a replicate) represents a topic-by-topic random
% permutation of a full |measuredRunSet|; each row contains the linear
% indexes needed to produce such permutation.

%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2018a or higher
% * *Copyright:* (C) 2018 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


%%
function [] = compute_discriminativePower_atPoolSamples(trackID, downsampling, startMeasure, endMeasure)

    % check that we have the correct number of input arguments.
    narginchk(2, 4);

    % setup common parameters
    common_parameters;

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

     % check that downsampling is a non-empty string
    validateattributes(downsampling, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'downsampling');

    if iscell(downsampling)
        % check that downsampling is a cell array of strings with one element
        assert(iscellstr(downsampling) && numel(downsampling) == 1, ...
            'MATTERS:IllegalArgument', 'Expected downsampling to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    downsampling = char(strtrim(downsampling));
    downsampling = downsampling(:).';
    
    % check that downsampling assumes a valid value
    validatestring(downsampling, ...
        EXPERIMENT.analysis.poolDownsample.downsampling.list, '', 'downsampling');
    
    if nargin == 4
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else 
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end
    
    % try to load existing replicates, if any
    try
        repID = EXPERIMENT.pattern.identifier.discPowReplicates(trackID);
        
        % load the replicated if any
        serload2(EXPERIMENT.pattern.file.analysis(trackID, repID), ...
            'WorkspaceVarNames', {'replicates'}, ...
            'FileVarNames', {repID});

        supplied.Replicates = true;
    catch ME
        supplied.Replicates = false;
    end
    
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing discriminative power at pool samples on track %s (%s) ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - downsampling type: %s\n', EXPERIMENT.analysis.poolDownsample.downsampling.(downsampling));    
    fprintf('  - slice \n');
    fprintf('    * start measure: %d\n', startMeasure);
    fprintf('    * end measure: %d\n', endMeasure);
   

    fprintf('+ Computing discriminative power \n');

    % for each measure
    for m = startMeasure:endMeasure
        
        mid = EXPERIMENT.measure.list{m};
        
        measureID = EXPERIMENT.pattern.identifier.downsampledMeasure(downsampling, mid, trackID);
                        
        fprintf('  - analysing %s\n', measureID);

        % load the measure in the tmp variable
        serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
            'WorkspaceVarNames', {'measuredRunSet'}, ...
            'FileVarNames', {measureID});

        iterations = measuredRunSet.Properties.UserData.iterations;

        % check that measured run set is a non-empty table
        validateattributes(measuredRunSet, {'table'}, {'nonempty'}, '', 'measuredRunSet', 1);

        sampleNum = length(measuredRunSet.Properties.UserData.sampleSize) + 1;

        % the ASL at different iterations and pool samples
        asl = cell2table(cell(iterations, sampleNum));
        asl.Properties.UserData.identifier = measuredRunSet.Properties.UserData.identifier;
        asl.Properties.UserData.pool = measuredRunSet.Properties.UserData.pool;
        asl.Properties.UserData.funcName = measuredRunSet.Properties.UserData.funcName;
        asl.Properties.UserData.downsampling = measuredRunSet.Properties.UserData.downsampling;
        asl.Properties.UserData.shortDownsampling = measuredRunSet.Properties.UserData.shortDownsampling;
        asl.Properties.UserData.name = measuredRunSet.Properties.UserData.name;
        asl.Properties.UserData.shortName = measuredRunSet.Properties.UserData.shortName;
        asl.Properties.UserData.sampleSize = measuredRunSet.Properties.UserData.sampleSize;
        asl.Properties.UserData.iterations = measuredRunSet.Properties.UserData.iterations;
        %         asl.Properties.UserData.ciAlpha = ciAlpha;
        asl.Properties.RowNames = measuredRunSet.Properties.RowNames;
        asl.Properties.VariableNames= measuredRunSet{1, 1}{1, 1}.Properties.VariableNames;

        dp = array2table(NaN(iterations, sampleNum));
        dp.Properties.UserData.identifier = measuredRunSet.Properties.UserData.identifier;
        dp.Properties.UserData.pool = measuredRunSet.Properties.UserData.pool;
        dp.Properties.UserData.funcName = measuredRunSet.Properties.UserData.funcName;
        dp.Properties.UserData.downsampling = measuredRunSet.Properties.UserData.downsampling;
        dp.Properties.UserData.shortDownsampling = measuredRunSet.Properties.UserData.shortDownsampling;
        dp.Properties.UserData.name = measuredRunSet.Properties.UserData.name;
        dp.Properties.UserData.shortName = measuredRunSet.Properties.UserData.shortName;
        dp.Properties.UserData.sampleSize = measuredRunSet.Properties.UserData.sampleSize;
        dp.Properties.UserData.iterations = measuredRunSet.Properties.UserData.iterations;
        %         dp.Properties.UserData.ciAlpha = ciAlpha;
        dp.Properties.RowNames = measuredRunSet.Properties.RowNames;
        dp.Properties.VariableNames= measuredRunSet{1, 1}{1, 1}.Properties.VariableNames;

        delta = array2table(NaN(iterations, sampleNum));
        delta.Properties.UserData.identifier = measuredRunSet.Properties.UserData.identifier;
        delta.Properties.UserData.pool = measuredRunSet.Properties.UserData.pool;
        delta.Properties.UserData.funcName = measuredRunSet.Properties.UserData.funcName;
        delta.Properties.UserData.downsampling = measuredRunSet.Properties.UserData.downsampling;
        delta.Properties.UserData.shortDownsampling = measuredRunSet.Properties.UserData.shortDownsampling;
        delta.Properties.UserData.name = measuredRunSet.Properties.UserData.name;
        delta.Properties.UserData.shortName = measuredRunSet.Properties.UserData.shortName;
        delta.Properties.UserData.sampleSize = measuredRunSet.Properties.UserData.sampleSize;
        delta.Properties.UserData.iterations = measuredRunSet.Properties.UserData.iterations;
        %         delta.Properties.UserData.ciAlpha = ciAlpha;
        delta.Properties.RowNames = measuredRunSet.Properties.RowNames;
        delta.Properties.VariableNames= measuredRunSet{1, 1}{1, 1}.Properties.VariableNames;


        % compute the discriminative power for each iteration
        for k = 1:iterations

            % compute the discriminative power for each sample
            for s = 1:sampleNum

                % the original pool is present only in the first column of the
                % first iteration, skip senseless computations afterwards
                if s == 1 && k > 1
                    asl{k, s} = asl{1, 1};
                    dp{k, s} = dp{1, 1};
                    delta{k, s} = delta{1, 1};
                    continue;
                end;

                % prepare the input to discriminative power by extracting the
                % measured run sets for a given pool sample and iteration


                tmp = measuredRunSet{k, 1}{1, 1}{:, s};
                tmp = vertcat(tmp{:, :});

                % the name is the short name of the measure plus the sample in
                % order to differentiate, e.g., between AP at pool sample 100%
                % and AP at pool sample 50%
                tmp.Properties.UserData.shortName = [measuredRunSet{k, 1}{1, 1}.Properties.UserData.shortName '_' ...
                    measuredRunSet{k, 1}{1, 1}.Properties.VariableNames{s}];

                input={tmp};


                % in the first iteration, we need to check whether we have
                % input replicates or we need to obtain them from discriminative
                % power for the first time, while in the following iterations
                % we will always use the already existing replicates
                if s == 1 && k == 1
                    if supplied.Replicates
                        [currentAsl, currentDp, currentDelta] = EXPERIMENT.analysis.dpow.compute.replicates(input, replicates);
                    else
                        [currentAsl, currentDp, currentDelta, replicates] = EXPERIMENT.analysis.dpow.compute.noReplicates(input);
                        
                        % save replicates
                        repID = EXPERIMENT.pattern.identifier.discPowReplicates(trackID);
                              
                        sersave2(EXPERIMENT.pattern.file.analysis(trackID, repID), ...
                            'WorkspaceVarNames', {'replicates'}, ...
                            'FileVarNames', {repID});

                        supplied.Replicates = true;                        
                    end;

                    % set also some general properties we do not know until the
                    % first run of discriminative power
                    asl.Properties.UserData.method = currentAsl.Properties.UserData.method;
                    asl.Properties.UserData.shortMethod = currentAsl.Properties.UserData.shortMethod;
                    asl.Properties.UserData.alpha = currentAsl.Properties.UserData.alpha;
                    asl.Properties.UserData.replicates = currentAsl.Properties.UserData.replicates;

                    dp.Properties.UserData.method = currentDp.Properties.UserData.method;
                    dp.Properties.UserData.shortMethod = currentDp.Properties.UserData.shortMethod;
                    dp.Properties.UserData.alpha = currentDp.Properties.UserData.alpha;
                    dp.Properties.UserData.replicates = currentDp.Properties.UserData.replicates;

                    delta.Properties.UserData.method = currentDelta.Properties.UserData.method;
                    delta.Properties.UserData.shortMethod = currentDelta.Properties.UserData.shortMethod;
                    delta.Properties.UserData.alpha = currentDelta.Properties.UserData.alpha;
                    delta.Properties.UserData.replicates = currentDelta.Properties.UserData.replicates;

                else
                    [currentAsl, currentDp, currentDelta] = EXPERIMENT.analysis.dpow.compute.replicates(input, replicates);
                end

                asl{k, s} = currentAsl{1,1};
                dp{k, s} = currentDp{:,:};
                delta{k, s} = currentDelta{:,:};

            end
        end
        
        dpowID = EXPERIMENT.pattern.identifier.downsampledDiscPower(downsampling, mid, trackID);

        aslID = EXPERIMENT.pattern.identifier.discPow.asl(downsampling, mid, trackID);
        dpID = EXPERIMENT.pattern.identifier.discPow.dp(downsampling, mid, trackID);
        deltaID = EXPERIMENT.pattern.identifier.discPow.delta(downsampling, mid, trackID);
               
        sersave2(EXPERIMENT.pattern.file.analysis(trackID, dpowID), ...
            'WorkspaceVarNames', {'asl','dp','delta'}, ...
            'FileVarNames', {aslID,dpID,deltaID});
        
        clear('measuredRunSet','asl','dp','delta');
    end
    
    fprintf('\n\n######## Total elapsed time for computing discriminative power at pool samples on track %s (%s):\n %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;

end

%%
