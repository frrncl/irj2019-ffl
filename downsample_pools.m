%% downsample_pools
% 
% Downsamples the pools of the collection, and saves them to a 
% |.mat| file.
%
%% Synopsis
%
%   [] = downsample_pools(downsampling)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track whose pool has to be downsampled.
% * *|downsampling|* - the type of downsampling to be applied.
%
% *Returns*
%
% Nothing

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2018a or higher
% * *Copyright:* (C) 2018 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%
%%
function [] = downsample_pools(trackID, downsampling)

    % check the number of input arguments
    narginchk(2, 2);

    % set up the common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    % check that downsampling is a non-empty string
    validateattributes(downsampling, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'downsampling');

    if iscell(downsampling)
        % check that downsampling is a cell array of strings with one element
        assert(iscellstr(downsampling) && numel(downsampling) == 1, ...
            'MATTERS:IllegalArgument', 'Expected downsampling to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    downsampling = char(strtrim(downsampling));
    downsampling = downsampling(:).';
    
    % check that downsampling assumes a valid value
    validatestring(downsampling, ...
        EXPERIMENT.analysis.poolDownsample.downsampling.list, '', 'downsampling');
    
    % start of overall import
    startImport = tic;
    
    fprintf('\n\n######## Downsample pools ########\n\n');

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - downsampling type: %s\n', EXPERIMENT.analysis.poolDownsample.downsampling.(downsampling));
    fprintf('  - pool reduction rates: %s\n', num2str(EXPERIMENT.analysis.poolDownsample.reductionRates, '%d%% '));
    fprintf('  - iterations: %d\n\n', EXPERIMENT.analysis.poolDownsample.iterations);

    % start of overall computations
    start = tic;
    fprintf('+ Loading the dataset\n');
    
    poolID = EXPERIMENT.pattern.identifier.pool(trackID);
    serload2(EXPERIMENT.pattern.file.dataset(trackID, poolID), ...
        'WorkspaceVarNames', {'pool'}, ...
        'FileVarNames', {poolID});
    
    fprintf('  - elapsed time: %s\n\n', elapsedToHourMinutesSeconds(toc(start)));   

    start = tic;
    fprintf('+ Downsampling the pool\n');            
    
    downsampled_PoolID = EXPERIMENT.pattern.identifier.downsampledPool(downsampling, trackID);
    downsampled_Pool = EXPERIMENT.analysis.poolDownsample.compute(pool, downsampling);
            
    fprintf('  - elapsed time: %s\n\n', elapsedToHourMinutesSeconds(toc(start)));

    start = tic;
    fprintf('+ Saving the downsampled pools\n');
    
    sersave2(EXPERIMENT.pattern.file.dataset(trackID, downsampled_PoolID), ...
                'WorkspaceVarNames', {'downsampled_Pool'}, ...
                'FileVarNames', {downsampled_PoolID});
    
    fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
    
    fprintf('\n\n######## Total elapsed time for downsampling pool %s (%s): %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startImport)));

    % enable warnings
    warning('on');