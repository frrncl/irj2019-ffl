%% rq0_anova_summary_report
% 
% Reports a summary about the different RQ0 analyses for the given
% track and saves them to a |.tex| file.
%
%% Synopsis
%
%   [] = rq0_anova_summary_report(trackID, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2018a or higher
% * *Copyright:* (C) 2018 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = rq0_anova_summary_report(trackID, startMeasure, endMeasure)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq0';
    end
    
    % check the number of input parameters
    narginchk(1, 3);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
       
    if nargin == 3
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end    
     
    % start of overall computations
    startComputation = tic;
    
    
    fprintf('\n\n######## Reporting summary %s ANOVA analyses on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.anova.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.anova.(TAG).description);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
        
    fprintf('+ Printing the report\n');
    
    % the file where the report has to be written    
    reportID = EXPERIMENT.pattern.identifier.rep.summaryAnova(TAG, trackID);
    fid = fopen(EXPERIMENT.pattern.file.report(trackID, reportID), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    fprintf(fid, '\\title{Summary Report on %s ANOVA Analyses \\\\ on %s}\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name);
    
    fprintf(fid, '\\author{Nicola Ferro}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
    
    
    fprintf(fid, 'Settings:\n');
    fprintf(fid, '\\begin{itemize}\n');    

    fprintf(fid, '\\item track: %s -- %s \n', strrep(trackID, '_', '\_'), EXPERIMENT.track.(trackID).name);
    fprintf(fid, '\\begin{itemize}\n');        
    fprintf(fid, '\\item topics: %d \n', EXPERIMENT.track.(trackID).topics);
    fprintf(fid, '\\item runs: %d \n', EXPERIMENT.track.(trackID).runs);
    fprintf(fid, '\\end{itemize}\n');
   
    fprintf(fid, '\\item analysis type:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item %s: %s \n', TAG, EXPERIMENT.analysis.anova.(TAG).name);
    fprintf(fid, '\\item model: %s \\\\ \n', EXPERIMENT.analysis.anova.(TAG).glmm);
    fprintf(fid, '%s\n', EXPERIMENT.analysis.anova.(TAG).description);
    fprintf(fid, '\\item sum of squares type: %d\n', EXPERIMENT.analysis.anova.sstype);
    fprintf(fid, '\\item significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf(fid, '\\end{itemize}\n');
        
    fprintf(fid, '\\item analysed measures:\n');
    fprintf(fid, '\\begin{itemize}\n');
    for m = startMeasure:endMeasure        
        fprintf(fid, '\\item %s: %s\n', ...
            strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), EXPERIMENT.measure.getName(m));        
    end
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\vspace*{1em}Rule of thumb for effect size $\\hat{\\omega}_{\\langle fact\\rangle}^2$:\n');
    fprintf(fid, '\\begin{itemize}\n');    
    fprintf(fid, '\\item large effect: $\\hat{\\omega}_{\\langle fact\\rangle}^2 \\geq 0.14$\n');
    fprintf(fid, '\\item medium effect: $0.06 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.14$\n');
    fprintf(fid, '\\item small effect: $0.01 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.06$\n');
    fprintf(fid, '\\item negative values have to be considered as $0$\n');
    fprintf(fid, '\\end{itemize}\n');
    
        
    fprintf(fid, '\\newpage\n');
    
    fprintf(fid, '\\begin{table}[p] \n');
    % fprintf(fid, '\\tiny \n');
    fprintf(fid, '\\centering \n');
    %fprintf(fid, '\\hspace*{-6.5em} \n');
    
    fprintf(fid, '\\caption{Summary of model %s on track \\texttt{%s}. Each cell reports $\\hat{\\omega}^2_{\\langle\\cdot\\rangle}$ effect sizes and, within parentheses, the $p$-value.}\n', ...
        EXPERIMENT.analysis.anova.(TAG).glmm, strrep(trackID, '_', '\_'));
    
    fprintf(fid, '\\label{tab:dtl-%s}\n', trackID);
    
    fprintf(fid, '\\begin{tabular}{|l|r|r|} \n');
    
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '\\multicolumn{1}{|c}{\\textbf{Measure}} & \\multicolumn{1}{|c}{\\textbf{Topic Effect}} & \\multicolumn{1}{|c|}{\\textbf{System Effect}} \\\\ \n');
    
    fprintf(fid, '\\hline \n');
    
    % for each measure
    for m = startMeasure:endMeasure
        
        mid = EXPERIMENT.measure.list{m};
        
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, mid, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG, mid, trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG, mid, trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
            'WorkspaceVarNames', {'tbl', 'soa'}, ...
            'FileVarNames', {anovaTableID, anovaSoAID});
        
        fprintf(fid, '%s					& %5.4f (%3.2f)			& %5.4f (%3.2f) \\\\', ...
            strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), ...
            soa.omega2p.subject, tbl{2, 7}, ...
            soa.omega2p.factorA,  tbl{3, 7});
        
        fprintf(fid, '\\hline \n');
        
    end % for measure
    
    fprintf(fid, '\\end{tabular} \n');
    
    fprintf(fid, '\\end{table} \n\n');
    
    
    fprintf(fid, '\\end{document} \n\n');
    
    fclose(fid);
    
    fprintf('\n\n######## Total elapsed time for reporting summary %s ANOVA analyses on track %s (%s): %s ########\n\n', ...
            TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
