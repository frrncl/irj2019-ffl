%% plotDiscriminativePowerAtPoolSamples
%
% It plot the discriminative power of a set of runs at differente pool samples 
% for only one iteration.
%
%% Synopsis
%
% plotDiscriminativePowerAtPoolSamples(varargin)
%
% 
% *Name-Value Pair Arguments*
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the
% argument name and |Value| is the corresponding value. |Name| must appear
% inside single quotes (' '). You can specify several name and value pair
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
%
% * *|Observed|* (mandatory) - a cell vector of strings containing the list
% of measure short names to be considered as observations.
% * *|TrackID|* (mandatory) - the identifier of the track to process.
% * *|Downsampling|* (mandatory) - the type of downsampling to be applied.
% * *|Reference|* (optional) -  a cell vector of strings containing the list
% of measure short names to be considered as references.
% Reference performances will be plot with a different color and line style
% with respect to observed performances.
% * *|DpStats|* (optional) -   a boolean indicating whether self
% discriminative powers have to be add to the legend. The default is |false|.
% * *|DeltaStats|* (optional) -   a boolean indicating whether deltas
% have to be add to the legend. The default is |false|.
% * *|SelfPlot|* (optional) -  a cell vector of strings containing the list
% of measure short names for which the self discriminative power plots
% have to be plotted. 

%
%% Example of use
%
%   plotDiscriminativePowerAtPoolSamples('Observed', {'P_10', 'P_100', 'Rprec'}, ...
%                  'Reference', {'AP', 'bpref', 'RBP_080'}, ....
%                  'TrackID', 'T13',...
%                  'Downsampling', 'srs', ...
%                  'SelfPlot', {'AP'},...            
%                  'DpStats', true)
%
% 
% You can note as |Observed| performances are plotted in blu with a
% continuous line while |Reference| performance are plotted in black with a
% dashed line.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2018b or higher
% * *Copyright:* (C) 2018 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = plotDiscriminativePowerAtPoolSamples(varargin)

    common_parameters

    persistent MARKERS ...
        REF_LINESTYLE REF_LINEWIDTH REF_MARKERSIZE ...
        OBS_LINESTYLE OBS_LINEWIDTH OBS_MARKERSIZE ...
        X_LIM_STEP;

    if isempty(MARKERS)

        %MARKERS = {'o', '+', '*', 'x', 's', 'd', '^', 'v', '>', '<', 'p', 'h'};

        MARKERS = {'^', '+', '*', 'v', 'x', 'd', 'o', 's', '>', 'p', 'x', 'h'};
        
        REF_LINESTYLE = '--';
        REF_LINEWIDTH = 1.0;
        REF_MARKERSIZE = 6;
        
        OBS_LINESTYLE = '-';
        OBS_LINEWIDTH = 1.2;
        OBS_MARKERSIZE = 7;
        
        X_LIM_STEP = 50;
    end
    
    % check that we have the correct number of input arguments.
    narginchk(1, inf);
    
    % parse the variable inputs
    pnames = {'Reference', 'Observed', 'DpStats', 'DeltaStats', 'SelfPlot', 'TrackID', 'Downsampling'};
    dflts =  {[]           []          false       false        []           []        []};
    
    if verLessThan('matlab', '9.2.0')
        [reference, observed, dpStats, deltaStats, selfPlot, trackID, downsampling, supplied] = matlab.internal.table.parseArgs(pnames, dflts, varargin{:});
    else
        [reference, observed, dpStats, deltaStats, selfPlot, trackID, downsampling, supplied] = matlab.internal.datatypes.parseArgs(pnames, dflts, varargin{:});
    end
    
    if supplied.TrackID
        % check that trackID is a non-empty string
        validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
        
        if iscell(trackID)
            
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(trackID) && numel(trackID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        trackID = char(strtrim(trackID));
        trackID = trackID(:).';
        
        % check that trackID assumes a valid value
        validatestring(trackID, ...
            EXPERIMENT.track.list, '', 'trackID');
    else
        error('MATTERS:MissingArgument', 'Parameter ''TrackID'' not provided: the identifier of the track is mandatory.');
    end
    
    if supplied.Downsampling
        % remove useless white spaces, if any, and ensure it is a char row
        downsampling = char(strtrim(downsampling));
        downsampling = downsampling(:).';
        
        % check that downsampling assumes a valid value
        validatestring(downsampling, ...
            EXPERIMENT.analysis.poolDownsample.downsampling.list, '', 'downsampling');
        
    else
        error('MATTERS:MissingArgument', 'Parameter ''Downsampling'' not provided: the type of applied downsampling is mandatory.');
    end
    
    
    if supplied.Observed
        % check that observed is a cell array
        validateattributes(observed, {'cell'}, {'nonempty', 'vector'}, '', 'Observed');
        
        for k = 1:length(observed)
            % check that each element is a non-empty table
            validateattributes(observed{k}, {'char'}, {'nonempty'}, '', num2str(k, 'Observed{%d}'), 1);
            observed{k} = char(strtrim(observed{k}));
        end
    else
        error('MATTERS:MissingArgument', 'Parameter ''Observed'' not provided: the observed performances are mandatory.');
    end
    
    % markers for the observed performances
    obsMarkers = MARKERS;
    
    % the labels for the legend, as deduced from the short name of the
    % computed measure
    measures = observed;
    % remove useless white spaces from the title, if any, and ensure it is a char row
    plotTitle = char(strtrim(EXPERIMENT.track.(trackID).name));
    plotTitle = plotTitle(:).';
    plotTitle = strrep(plotTitle, '_', '\_');
    
    aslData=cell(length(observed),1);
    dpData=cell(length(observed),1);
    deltaData=cell(length(observed),1);
    
    for k=1:length(observed)
        
        dpowID = EXPERIMENT.pattern.identifier.downsampledDiscPower(downsampling, observed{k}, trackID);
        
        aslID = EXPERIMENT.pattern.identifier.discPow.asl(downsampling, observed{k}, trackID);
        dpID = EXPERIMENT.pattern.identifier.discPow.dp(downsampling, observed{k}, trackID);
        deltaID = EXPERIMENT.pattern.identifier.discPow.delta(downsampling, observed{k}, trackID);
        
        
        
        serload2(EXPERIMENT.pattern.file.analysis(trackID, dpowID), ...
            'WorkspaceVarNames', {'asl', 'dp', 'delta'}, ...
            'FileVarNames', {aslID, dpID, deltaID});
        aslData{k} = asl;
        dpData{k} = dp;
        deltaData{k} = delta;
        
        if (k == 1 && asl.Properties.UserData.iterations ~= 1)
            error('MATTERS:IllegalArgument', 'Expected number of iterations to be equal to 1.');
        end
        
        clear('asl','dp','delta');
    end
    
    
    if supplied.Reference
        % check that reference is a cell array
        validateattributes(reference, {'cell'}, {'nonempty', 'vector'}, '', 'Reference');
        
        aslDataRef=cell(length(reference),1);
        dpDataRef=cell(length(reference),1);
        deltaDataRef=cell(length(reference),1);
        
        for k = 1:length(reference)
            % check that each element is a non-empty table
            validateattributes(reference{k}, {'char'}, {'nonempty'}, '', num2str(k, 'Reference{%d}'), 1);
            
            dpowID = EXPERIMENT.pattern.identifier.downsampledDiscPower(downsampling, reference{k}, trackID);
            
            aslID = EXPERIMENT.pattern.identifier.discPow.asl(downsampling, reference{k}, trackID);
            dpID = EXPERIMENT.pattern.identifier.discPow.dp(downsampling, reference{k}, trackID);
            deltaID = EXPERIMENT.pattern.identifier.discPow.delta(downsampling, reference{k}, trackID);
            
            serload2(EXPERIMENT.pattern.file.analysis(trackID, dpowID), ...
                'WorkspaceVarNames', {'asl', 'dp', 'delta'}, ...
                'FileVarNames', {aslID, dpID, deltaID});
            aslDataRef{k} = asl;
            dpDataRef{k} = dp;
            deltaDataRef{k} = delta;
            
            clear('asl','dp','delta');
            
        end

        % do not use more than half of the markers for the reference
        % performances
        refMarkers = MARKERS(1:min(length(reference), length(MARKERS)/2));

        % assign all the remaining markers for the observed performances
        obsMarkers = MARKERS(length(refMarkers)+1:end);

        % add the short name of the measure as label for the plot
        measures = [measures reference];

    end
    
    if supplied.DpStats
        % check that selfPlot is a non-empty scalar logical value
        validateattributes(dpStats, {'logical'}, {'nonempty', 'scalar'}, '', 'DpStats');
    end
    
    if supplied.DeltaStats
        % check that selfPlot is a non-empty scalar logical value
        validateattributes(deltaStats, {'logical'}, {'nonempty', 'scalar'}, '', 'DeltaStats');
    end
    
    
    samples = length(aslData{1}.Properties.UserData.sampleSize) + 1;
    colors = hsv(samples);
    
    if supplied.SelfPlot
        % check that observed is a cell array
        validateattributes(selfPlot, {'cell'}, {'nonempty', 'vector'}, '', 'Observed');
        
        for k = 1:length(selfPlot)
            % check that each element is a non-empty table
            validateattributes(selfPlot{k}, {'char'}, {'nonempty'}, '', num2str(k, 'Observed{%d}'), 1);
            
            dpowID = EXPERIMENT.pattern.identifier.downsampledDiscPower(downsampling, selfPlot{k}, trackID);
            
            aslID = EXPERIMENT.pattern.identifier.discPow.asl(downsampling, selfPlot{k}, trackID);
            dpID = EXPERIMENT.pattern.identifier.discPow.dp(downsampling, selfPlot{k}, trackID);
            deltaID = EXPERIMENT.pattern.identifier.discPow.delta(downsampling, selfPlot{k}, trackID);
            
            serload2(EXPERIMENT.pattern.file.analysis(trackID, dpowID), ...
                'WorkspaceVarNames', {'asl', 'dp', 'delta'}, ...
                'FileVarNames', {aslID, dpID, deltaID});
            
            DataAsl{k} = asl;
            DataDp{k} = dp;
            DataDelta{k} = delta;
            
            clear('asl','dp','delta');
        end
        
        % for each measure produce a plot of the ASL of the measure at
        % different pool samples
        for m = 1:length(selfPlot)
            
            h = figure('Visible', 'off');
            hold on
            
            legendLabels = cell(1, samples);
            
            % plot the observed performances
            for k = 1:samples
                Asl = DataAsl{m}{1, k}{1, 1}{:, :};
                [Asl loc] = sort(Asl(~isnan(Asl)), 1, 'descend');
                
                plot(Asl, 'Color', colors(k, :), 'LineStyle', OBS_LINESTYLE, ...
                    'Marker', MARKERS{mod(k, length(MARKERS)) + 1}, ...
                    'LineWidth', OBS_LINEWIDTH, 'MarkerSize', OBS_MARKERSIZE, ...
                    'MarkerFaceColor', colors(k, :));
                
                
                if k == 1
                    legendLabels{k} = [selfPlot{m} ', original'];
                else
                    switch lower(DataAsl{m}.Properties.UserData.shortDownsampling)
                        case {'srs', 'rs'}
                            legendLabels{k} = sprintf('%s, %d%% sample', selfPlot{m}, ...
                                DataAsl{m}.Properties.UserData.sampleSize(k-1));
                        case 'pds'
                            legendLabels{k} = sprintf('%s, %d% depth', selfPlot{m}, ...
                                DataAsl{m}.Properties.UserData.sampleSize(k-1));
                        otherwise
                            error('MATTERS:IllegalArgument', 'Unrecognized pool downsampling method %s. Only PoolDepthSampling, RandomSampling, and StratifiedRandomSampling are allowed', ...
                                DataAsl{m}.Properties.UserData.downsampling);
                    end
                end
                
                % adjust the legend, if needed
                if dpStats || deltaStats
                    tmpLegendLabels = [legendLabels{k} ' ['];
                    
                    if dpStats
                        tmpLegendLabels = sprintf('%sDP = %4.2f%%', tmpLegendLabels, ...
                            DataDp{m}{1, k}*100);
                    end
                    
                    if deltaStats
                        if dpStats
                            tmpLegendLabels = [tmpLegendLabels '; '];
                        end
                        
                        tmpLegendLabels = sprintf('%s\\Delta = %5.2f', tmpLegendLabels, ...
                            DataDelta{m}{1,k});
                        
                    end
                    
                    legendLabels{k} = [tmpLegendLabels ']'];
                end
                
            end
            
            set(gca, 'FontSize', 14);
            
            set(gca, 'XLim', [0 (fix(length(Asl)/X_LIM_STEP) + 1)*X_LIM_STEP])
            
            set(gca, 'YLim', [0 0.15], ...
                'YTick', [0:0.01:0.15], ...
                'YTickLabel', cellstr(num2str([0:0.01:0.15].', '%3.2f')).');
            
            grid on;
            
            legendLabels = strrep(legendLabels, '_', '\_');
            hl = legend(legendLabels{:}, 'Location', 'NorthEast');
            set(hl, 'FontSize', 12);
            
            xlabel('System Pair (sorted by decreasing ASL)')
            ylabel('Achieved Significance Level (ASL)')
            title(plotTitle);
            
            
            set(h,'PaperPositionMode','manual');
            set(h,'PaperUnits','normalized');
            set(h,'PaperPosition',[0.05 0.05 0.9 0.9]);
            set(h,'PaperType','A4');
            set(h,'PaperOrientation','landscape');
            
            figureID = EXPERIMENT.pattern.identifier.discPow.general('asl_atPoolSamples', downsampling, measures{m}, trackID);
            print(h, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));
            close(h);
            
        end
        
        clear('DataAsl','DataDp','DataDelta')
    end
    
    switch lower(aslData{1}.Properties.UserData.shortMethod)
        case {'pbt'}
            plotTitle = {plotTitle; 'ASL using Paired Bootstrap Test'};
        case 'rthsdt'
            plotTitle = {plotTitle; 'ASL using Randomised Tukey HSD Test'};
        otherwise
            error('MATTERS:IllegalArgument', 'Unrecognized discriminative power method %s. Only Paired Bootstrap Test and Randomised Tukey HSD Test are allowed', ...
                aslData{1}.Properties.UserData.method);
    end
    
    measures = [observed reference];
    obsColors = parula(length(observed)+2);
    refColors = bone(length(reference)+2);
    origPlotTitle = plotTitle{2};
    
    for k = 1:samples
        
        if k == 1
            plotTitle{2} = [origPlotTitle ', Original Pool'];
            sfx = sprintf('Original');
        else
            switch lower(aslData{1}.Properties.UserData.shortDownsampling)
                case {'srs', 'rs'}
                    plotTitle{2} = sprintf('%s, %d%% Pool Reduction Rate', origPlotTitle, ...
                        aslData{1}.Properties.UserData.sampleSize(k-1));
                    
                    sfx = sprintf('PoolDepth_%d', aslData{1}.Properties.UserData.sampleSize(k-1));
                case 'pds'
                    plotTitle{2} = sprintf('%s, %d% Pool Depth', origPlotTitle, ...
                        aslData{1}.Properties.UserData.sampleSize(k-1));
                    sfx = sprintf('PoolDepth_%d', aslData{1}.Properties.UserData.sampleSize(k-1));
                otherwise
                    error('MATTERS:IllegalArgument', 'Unrecognized pool downsampling method %s. Only PoolDepthSampling, RandomSampling, and StratifiedRandomSampling are allowed', ...
                        aslData{1}.Properties.UserData.downsampling);
            end
        end
        
        legendLabels = measures;
        
        legendLabels = strrep(legendLabels, '_', '\_');
        
        
        h = figure('Visible', 'off');
        hold on
        
        % plot the observed performances
        for m = 1:length(observed)
            
            Asl = aslData{m}{1, k}{1, 1}{:, :};
            [Asl loc] = sort(Asl(~isnan(Asl)), 1, 'descend');
            
            plot(Asl, 'Color', obsColors(m, :), 'LineStyle', OBS_LINESTYLE, ...
                'Marker', obsMarkers{mod(m, length(obsMarkers)) + 1}, ...
                'LineWidth', OBS_LINEWIDTH, 'MarkerSize', OBS_MARKERSIZE, ...
                'MarkerFaceColor', obsColors(m, :));
            
            % adjust the legend, if needed
            if dpStats || deltaStats
                
                legendLabels{m} = [legendLabels{m} ' ['];
                
                if dpStats
                    legendLabels{m} = sprintf('%sDP = %4.2f%%', legendLabels{m}, ...
                        dpData{m}{1, k}*100);
                end
                
                if deltaStats
                    if dpStats
                        legendLabels{m} = [legendLabels{m} '; '];
                    end
                    
                    legendLabels{m} = sprintf('%s\\Delta = %4.2f', legendLabels{m}, ...
                        deltaData{m}{1, k});
                end
                legendLabels{m} = [legendLabels{m} ']'];
                
            end
        end
        
        % plot the reference performances
        for m = 1:length(reference)
            
            Asl = aslDataRef{m}{1, k}{1, 1}{:, :};
            [Asl loc] = sort(Asl(~isnan(Asl)), 1, 'descend');
            
            plot(Asl, 'Color', refColors(m, :), 'LineStyle', REF_LINESTYLE, ...
                'Marker', refMarkers{mod(m, length(refMarkers)) + 1}, ...
                'LineWidth', REF_LINEWIDTH, 'MarkerSize', REF_MARKERSIZE, ...
                'MarkerFaceColor', refColors(m, :));
            
            % adjust the legend, if needed
            if dpStats || deltaStats
                
                legendLabels{m+length(observed)} = [legendLabels{m+length(observed)} ' ['];
                
                if dpStats
                    legendLabels{m+length(observed)} = sprintf('%sDP = %4.2f%%', legendLabels{m+length(observed)}, ...
                        dpDataRef{m}{1, k}*100);
                end
                
                if deltaStats
                    if dpStats
                        legendLabels{m+length(observed)} = [legendLabels{m+length(observed)} '; '];
                    end
                    
                    legendLabels{m+length(observed)} = sprintf('%s\\Delta = %4.2f', legendLabels{m+length(observed)}, ...
                        deltaDataRef{m}{1, k});
                end
                
                legendLabels{m+length(observed)} = [legendLabels{m+length(observed)} ']'];
            end
            
        end
        
        set(gca, 'FontSize', 14);
        
        set(gca, 'XLim', [0 (fix(length(Asl)/X_LIM_STEP) + 1)*X_LIM_STEP])
        
        set(gca, 'YLim', [0 0.15], ...
            'YTick', [0:0.01:0.15], ...
            'YTickLabel', cellstr(num2str([0:0.01:0.15].', '%3.2f')).');
        
        grid on;
        
        hl = legend(legendLabels{:}, 'Location', 'NorthEast');
        set(hl, 'FontSize', 12);
        
        xlabel('System Pair (sorted by decreasing ASL)')
        ylabel('Achieved Significance Level (ASL)')
        title(plotTitle);
        
        set(h,'PaperPositionMode','manual');
        set(h,'PaperUnits','normalized');
        set(h,'PaperPosition',[0.05 0.05 0.9 0.9]);
        set(h,'PaperType','A4');
        set(h,'PaperOrientation','landscape');
        
        
        figureID = EXPERIMENT.pattern.identifier.discPow.general('asl', downsampling, sfx, trackID);
        print(h, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));
        close(h);
    end
    
    h = figure('Visible', 'off');
    hold on
    
    legendLabels = measures;
    
    legendLabels = strrep(legendLabels, '_', '\_');
    
    % plot the observed performances
    for m = 1:length(observed)
        
        dp = dpData{m}{:, :};
        
        plot(1:length(dp), dp, 'Color', obsColors(m, :), 'LineStyle', OBS_LINESTYLE, ...
            'Marker', obsMarkers{mod(m, length(obsMarkers)) + 1}, ...
            'LineWidth', 3, 'MarkerSize', OBS_MARKERSIZE, ...
            'MarkerFaceColor', obsColors(m, :));
        
        
        % adjust the legend, if needed
        %         if dpStats
        %             legendLabels{m} = [legendLabels{m} ' ['];
        %
        %             legendLabels{m} = sprintf('%sDP = %4.2f%%', legendLabels{m}, ...
        %                 dp(1)*100);
        %
        %             legendLabels{m} = [legendLabels{m} ']'];
        %
        %         end
    end
    
    % plot the reference performances
    for m = 1:length(reference)
        
        dp = dpDataRef{m}{:, :};
        
        plot(1:length(dp), dp, 'Color', refColors(m, :), 'LineStyle', REF_LINESTYLE, ...
            'Marker', refMarkers{mod(m, length(refMarkers)) + 1}, ...
            'LineWidth', 2, 'MarkerSize', REF_MARKERSIZE, ...
            'MarkerFaceColor', refColors(m, :));
        
        % adjust the legend, if needed
        %         if dpStats
        %             legendLabels{m+length(observed)} = [legendLabels{m+length(observed)} ' ['];
        %
        %             legendLabels{m+length(observed)} = sprintf('%sDP = %4.2f%%', legendLabels{m+length(observed)}, ...
        %                 dp(1)*100);
        %
        %             legendLabels{m+length(observed)} = [legendLabels{m+length(observed)} ']'];
        %
        %         end
    end
    
    switch lower(dpData{1}.Properties.UserData.shortDownsampling)
        case 'srs'
            xTickLabels = [100 dpData{1}.Properties.UserData.sampleSize];
            xTickLabels = strtrim(cellstr(num2str(xTickLabels.', '%d%%')));
            xLabel = 'Pool Reduction Rate';
            plotTitle = {plotTitle,  'Discriminative Power with Stratified Random Pool Sampling  '};
        case 'rs'
            xTickLabels = [100 dpData{1}.Properties.UserData.sampleSize];
            xTickLabels = strtrim(cellstr(num2str(xTickLabels.', '%d%%')));
            xLabel = 'Pool Reduction Rate';
            plotTitle = {plotTitle,  'Discriminative Power with Random Pool Sampling  '};
        case 'pds'
            xTickLabels = [100 dpData{1}.Properties.UserData.sampleSize];
            xTickLabels = strtrim(cellstr(num2str(xTickLabels.', '%d%%')));
            xLabel = 'Pool Reduction Rate';
            plotTitle = {plotTitle,  'Discriminative Power with Pool Depth Sampling  '};
        otherwise
            error('MATTERS:IllegalArgument', 'Unrecognized pool downsampling method %s. Only PoolDepthSampling, RandomSampling, and StratifiedRandomSampling are allowed', ...
                dpData{1}.Properties.UserData.downsampling);
    end
    
    
    set(gca, 'FontSize', 18);
    set(gca,'XTick', 1:length(xTickLabels));
    set(gca,'XTickLabel', xTickLabels);
    
    set(gca, 'YLim', [0 1], ...
        'YTick', 0:0.1:1, ...
        'YTickLabel', strtrim(cellstr(num2str([0:10:100].', '%d%%'))));
    
    grid on;
    
    hl = legend(legendLabels{:}, 'Location', 'NorthEast');
    set(hl, 'FontSize', 12);
    
    xlabel(xLabel)
    ylabel('Discriminative Power')
    %    title(plotTitle);
    
    set(h,'PaperPositionMode','manual');
    set(h,'PaperUnits','normalized');
    set(h,'PaperPosition',[0.05 0.05 0.9 0.9]);
    set(h,'PaperType','A3');
    set(h,'PaperOrientation','landscape');
    
    
    figureID = EXPERIMENT.pattern.identifier.discPow.general('dp_atPoolSamples', downsampling, 'plot', trackID);
    print(h, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

end



