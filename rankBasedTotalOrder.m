%% rankBasedTotalOrder
% 
% Computes the rank-based total order (RBTO) measure as proposed by 
% [Ferrante et al., 2018]. RBTO works only with graded
% relevance judgments and it is not possible to ask for a mapping to binary
% relevance.

%% Synopsis
%
%   [measuredRunSet, poolStats, runSetStats, inputParams] = rankBasedTotalOrder(pool, runSet, Name, Value)
%  
% Note that rank-based total order will be NaN when there are no relevant
% documents for a given topic in the pool (this may happen due to the way
% in which relevance degrees are mapped to binary relevance).
%
% *Parameters*
%
% * *|pool|* - the pool to be used to assess the run(s). It is a table in the
% same format returned by <../io/importPoolFromFileTRECFormat.html 
% importPoolFromFileTRECFormat>;
% * *|runSet|* - the run(s) to be assessed. It is a table in the same format
% returned by <../io/importRunFromFileTRECFormat.html 
% importRunFromFileTRECFormat> or by <../io/importRunsFromDirectoryTRECFormat.html 
% importRunsFromDirectoryTRECFormat>;
%
% *Name-Value Pair Arguments*
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the 
% argument name and |Value| is the corresponding value. |Name| must appear 
% inside single quotes (' '). You can specify several name and value pair 
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
% * *|ShortNameSuffix|* (optional) - a string providing a suffix which will
% be concatenated to the short name of the measure. It can contain only
% letters, numbers and the underscore. The default is empty.
% * *|FixNumberRetrievedDocuments|* (optional) - an integer value
% specifying the expected length of the retrieved document list. Topics
% retrieving more documents than |FixNumberRetrievedDocuments| will be 
% truncated at |FixNumberRetrievedDocuments|; tocpis retrieving less
% documents than |FixNumberRetrievedDocuments| will be padded with
% additional not relevant documents according. If not specified, the 
% value 1,000 will be used as default, to mimic the behaviour of trec_eval.
% * *|MapToRelevanceWeights|* (optional) - a vector of numbers to which the
% relevance degrees in the pool will be mapped. It must be an increasing
% vector with as many elements as the relevance degrees in the pool are.
% The default is |[0, 1, 2, ...]| up to as many relevance degrees are in
% the pool.
% * *|Verbose|* (optional) - a boolean specifying whether additional
% information has to be displayed or not. If not specified, then |false| is 
% used as default.
%
% *Returns*
%
% * |measureRunSet|  - a table containing a row for each topic and a column 
% for each run named |runName|. Each cell of the table contains a scalar
% representing the RBTO. The |UserData| property of  the table 
% contains a struct  with the  following fields: _|identifier|_ is the 
% identifier of the run; _|name|_  is the name of the computed measure, i.e.
% |rankBasedTotalOrder|; _|shortName|_ is a short name of the computed 
% measure, i.e. |RBTO|; _|pool|_ is the identifier of the pool with respect 
% to which the measure has been computed.
% * *|poolStats|* - see description in <assess.html assess>.
% * *|runSetStats|* - see description in <assess.html assess>.
% * *|inputParams|* - a struct summarizing the input parameters passed.

%% Example of use
%  
%   measuredRunSet = rankBasedTotalOrder(pool, runSet);
%
% It computes the set-based total order values for a run set. Suppose the 
% run set contains the following runs:
% 
% * |JuruDes.txt|
% * |JuruDesAggr.txt|
%
% In this example each run has two topics, |301| and |302|. It returns the 
% following table.
%
%            JuruDes      JuruDesAggr
%           __________    ___________
%    301       12685       12676 
%    302       38576       38576 
%
% Column names are run identifiers, row names are topic identifiers; cells
% contain a row vector with the value of the rank-biased precision.
% 
%   JuruDes_301 = measuredRunSet{'301','JuruDes'}
%
%   ans =
%
%    12685
%
% It returns the set-based total order for topic 301 of run JuruDes.
%
%% References
% 
% Please refer to :
%
% * Ferrante, M., Ferro, N. and Pontarollo, S. (2018). A General Theory of
% IR Evaluation Measures. _ IEEE Transactions on Knowledge and Data 
% Engineering (TKDE)_.
% 
%
%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Author*: <mailto:elosiouk@math.unipd.it Eleonora Losiouk>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2018a or higher
% * *Copyright:* (C) 2018 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


%%
function [measuredRunSet, poolStats, runSetStats, inputParams] = rankBasedTotalOrder(pool, runSet, varargin)
    
    persistent UNSAMPLED_UNJUDGED;
           
    if isempty(UNSAMPLED_UNJUDGED)
        % New categorical value to be added to the pool to indicate not
        % sampled documents or documents that have been pooled but
        % unjudged. See Yilmaz and Aslam, CIKM 2006
        UNSAMPLED_UNJUDGED = 'U_U';
    end
    
    % check that we have the correct number of input arguments.
    narginchk(2, inf);
    
    % parse the variable inputs
    pnames = {'ShortNameSuffix', 'MapToRelevanceWeights'  'FixNumberRetrievedDocuments' 'Verbose'};
    dflts =  {[]                       []                  []                          false};
    
    if verLessThan('matlab', '9.2.0')
        [shortNameSuffix, mapToRelevanceWeights, fixNumberRetrievedDocuments, verbose, supplied] ...
            = matlab.internal.table.parseArgs(pnames, dflts, varargin{:});
    else
        [shortNameSuffix, mapToRelevanceWeights, fixNumberRetrievedDocuments, verbose, supplied] ...
            = matlab.internal.datatypes.parseArgs(pnames, dflts, varargin{:});
    end
    
    % actual parameters to be passed to assess.m, at least 6
    assessInput = cell(1, 8);
    
    % get the relevance degrees in the pools
    relevanceDegrees = categories(pool{:, 1}{1, 1}.RelevanceDegree);
    relevanceDegrees = relevanceDegrees(:).';
    
    if supplied.MapToRelevanceWeights
        validateattributes(mapToRelevanceWeights, {'numeric'}, ....
            {'vector', 'nonempty', 'increasing', 'numel', numel(relevanceDegrees)}, ...
            '', 'MapToRelevanceWeights');
        
        % ensure it is a row vector
        mapToRelevanceWeights = mapToRelevanceWeights(:).';
    else
        if strcmpi(relevanceDegrees{1}, UNSAMPLED_UNJUDGED)
            % set default 1-based relevance weights skipping U_U docs
            mapToRelevanceWeights = 0:1:1*(length(relevanceDegrees) - 2);
        else
            % set default 1-based relevance weights
            mapToRelevanceWeights = 0:1:1*(length(relevanceDegrees) - 1);
        end
    end
        
    % not assessed documents must be considered as not relevant for
    % precision
    assessInput{1, 1} = 'NotAssessed';
    assessInput{1, 2} = 'NotRelevant';
    
    % map to binary relevance weights to make follow-up computations
    % handier
    assessInput{1, 3} = 'MapToRelevanceWeights';
    assessInput{1, 4} = mapToRelevanceWeights;
    
    % padding is not needed
    assessInput{1, 5} = 'FixNumberRetrievedDocuments';
    assessInput{1, 6} = fixNumberRetrievedDocuments;
    
    % remove unsampled/unjudged documents because they are not appropriate
    % for average precision computation
    assessInput{1, 7} = 'RemoveUUFromPool';
    assessInput{1, 8} = true;
    
    if supplied.ShortNameSuffix
        if iscell(shortNameSuffix)
            % check that nameSuffix is a cell array of strings with one element
            assert(iscellstr(shortNameSuffix) && numel(shortNameSuffix) == 1, ...
                'MATTERS:IllegalArgument', 'Expected NameSuffix to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        shortNameSuffix = char(strtrim(shortNameSuffix));
        shortNameSuffix = shortNameSuffix(:).';
        
        % check that the nameSuffix is ok according to the matlab rules
        if ~isempty(regexp(shortNameSuffix, '\W*', 'once'))
            error('MATTERS:IllegalArgument', 'NameSuffix %s is not valid: it can contain only letters, numbers, and the underscore.', ...
                shortNameSuffix);
        end
        
        % if it starts with an underscore, remove it since il will be
        % appended afterwards
        if strcmp(shortNameSuffix(1), '_')
            shortNameSuffix = shortNameSuffix(2:end);
        end
    end
    
    if supplied.Verbose
        % check that verbose is a non-empty scalar logical value
        validateattributes(verbose, {'logical'}, {'nonempty','scalar'}, '', 'Verbose');
    end    
                     
    if verbose
        fprintf('\n\n----------\n');
        
        fprintf('Computing rankBasedTotalOrder for run set %s with respect to pool %s: %d run(s) and %d topic(s) to be processed.\n\n', ...
            runSet.Properties.UserData.identifier, pool.Properties.UserData.identifier, width(runSet), height(runSet));
    end
    
    [assessedRunSet, poolStats, runSetStats, inputParams] = assess(pool, runSet, 'Verbose', verbose, assessInput{:});
    
     % the topic currently under processing
    ct = 1;
        
    % the run currently under processing
    cr = 1;
    
    % compute the measure topic-by-topic
    measuredRunSet = rowfun(@processTopic, assessedRunSet, 'OutputVariableNames', runSet.Properties.VariableNames, 'OutputFormat', 'table', 'ExtractCellContents', true, 'SeparateInputs', false);
    measuredRunSet.Properties.UserData.identifier = assessedRunSet.Properties.UserData.identifier;
    measuredRunSet.Properties.UserData.pool = pool.Properties.UserData.identifier;
    
    measuredRunSet.Properties.UserData.name = 'rankBasedTotalOrder';
    measuredRunSet.Properties.UserData.shortName = 'RBTO';
    
    if ~isempty(shortNameSuffix)
        measuredRunSet.Properties.UserData.shortName = [measuredRunSet.Properties.UserData.shortName ...
            '_' shortNameSuffix];
    end

    if verbose
        fprintf('Computation of rankBasedTotalOrder completed.\n');
    end
    
    %%
    
    % compute the measure for a given topic over all the runs
    function [varargout] = processTopic(topic)
        
        if(verbose)
            fprintf('Processing topic %s (%d out of %d)\n', pool.Properties.RowNames{ct}, ct, inputParams.topics);
            fprintf('  - run(s): ');
        end
               
        % reset the index of the run under processing for each topic
        cr = 1;
         
        % compute the measure only on those column which contain the
        % actual runs
        varargout = cellfun(@processRun, topic);
              
        % increment the index of the current topic under processing
        ct = ct + 1;    
        
         if(verbose)
            fprintf('\n');
         end
        
        %% 
        
        % compute the measure for a given topic of a given run
        function [measure] = processRun(runTopic)
                              
            if(verbose)
                fprintf('%s ', runSet.Properties.VariableNames{cr});
            end
            
            % avoid useless computations when you already know that either
            % the run has retrieved no relevant documents (0) or that there
            % are no relevant documents in the pool (NaN)
            if(runSetStats{ct, cr}.relevantRetrieved == 0)                
                if (poolStats{ct, 'NotRelevant'} == poolStats{ct, 'Assessment'})
                    measure = {NaN};
                else
                    measure = {0};
                end
                
                % increment the index of the current run under processing
                cr = cr + 1;
               
                return;
            end
                    
            N = height(runTopic);

            n = runTopic{:, 'Assessment'}.';
            
            % consider only the relevant documents for computations
            idx = n > 0;
            
            n = n(idx);
            
            % this is c+1 of equation 7 since it already accounts for the
            % not relevant degree
            c = length(mapToRelevanceWeights);

            i = 1:N;
            i = i(idx);
                        
            measure = n .* c .^ (N-i);
            
            measure = sum(measure);
                                                               
            % properly wrap the results into a cell in order to fit it into
            % a value for a table
            measure = {measure};
            
            % increment the index of the current run under processing
            cr = cr + 1;
        end
    end
    
end