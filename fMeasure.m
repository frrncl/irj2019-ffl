%% fMeasures
% 
% Computes the F-measures. SBTO works only with binary relevance.

%% Synopsis
%
%   [measuredRunSet, poolStats, runSetStats, inputParams] = setBasedTotalOrder(pool, runSet, Name, Value)
%  
% Note that set- based total order will be NaN when there are no relevant
% documents for a given topic in the pool (this may happen due to the way
% in which relevance degrees are mapped to binary relevance).
%
% *Parameters*
%
% * *|pool|* - the pool to be used to assess the run(s). It is a table in the
% same format returned by <../io/importPoolFromFileTRECFormat.html 
% importPoolFromFileTRECFormat>;
% * *|runSet|* - the run(s) to be assessed. It is a table in the same format
% returned by <../io/importRunFromFileTRECFormat.html 
% importRunFromFileTRECFormat> or by <../io/importRunsFromDirectoryTRECFormat.html 
% importRunsFromDirectoryTRECFormat>;
%
% *Name-Value Pair Arguments*
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the 
% argument name and |Value| is the corresponding value. |Name| must appear 
% inside single quotes (' '). You can specify several name and value pair 
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
% * *|ShortNameSuffix|* (optional) - a string providing a suffix which will
% be concatenated to the short name of the measure. It can contain only
% letters, numbers and the underscore. The default is empty.
% * *|NotAssessed|* (optional) - a string indicating how not assessed
% documents, i.e. those in the run but not in the pool, have to be
% processed: |NotRevelant|, the minimum of the relevance degrees of the 
% pool is used as |NotRelevant|; |Condensed|, the not assessed documents 
% are  removed from the run. If not specified, the default value  is 
% |NotRelevant| to mimic the behaviour of trec_eval.
% * *|MapToRelevanceWeights|* (optional) - a vector of numbers to which the
% relevance degrees in the pool will be mapped. It must be an increasing
% vector with as many elements as the relevance degrees in the pool are.
% The default is |[0, 5, 10, ...]| up to as many relevance degrees are in
% the pool.
% * *|Verbose|* (optional) - a boolean specifying whether additional
% information has to be displayed or not. If not specified, then |false| is 
% used as default.
%
% *Returns*
%
% * |measureRunSet|  - a table containing a row for each topic and a column 
% for each run named |runName|. Each cell of the table contains a scalar
% representing the average precision. The |UserData| property of  the table 
% contains a struct  with the  following fields: _|identifier|_ is the 
% identifier of the run; _|name|_  is the name of the computed measure, i.e.
% |setBasedTotalOrder|; _|shortName|_ is a short name of the computed 
% measure, i.e. |SBTO|; _|pool|_ is the identifier of the pool with respect 
% to which the measure has been computed.
% * *|poolStats|* - see description in <assess.html assess>.
% * *|runSetStats|* - see description in <assess.html assess>.
% * *|inputParams|* - a struct summarizing the input parameters passed.

%% Example of use
%  
%   measuredRunSet = setBasedTotalOrder(pool, runSet);
%
% It computes the set-based total order values for a run set. Suppose the 
% run set contains the following runs:
% 
% * |JuruDes.txt|
% * |JuruDesAggr.txt|
%
% In this example each run has two topics, |301| and |302|. It returns the 
% following table.
%
%            JuruDes      JuruDesAggr
%           __________    ___________
%    301       0.12685       0.12676 
%    302       0.38576       0.38576 
%
% Column names are run identifiers, row names are topic identifiers; cells
% contain a row vector with the value of the rank-biased precision.
% 
%   JuruDes_301 = measuredRunSet{'301','JuruDes'}
%
%   ans =
%
%    0.12685
%
% It returns the set-based total order for topic 301 of run JuruDes.
%
%% References
% 
% Please refer to :
%
% * Ferrante, M., Ferro, N. and Pontarollo, S. (2018). A General Theory of
% IR Evaluation Measures. _ IEEE Transactions on Knowledge and Data 
% Engineering (TKDE)_.
% 
%
%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2018a or higher
% * *Copyright:* (C) 2018 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


%%
function [measuredRunSet, poolStats, runSetStats, inputParams] = fMeasure(pool, runSet, varargin)
    
    % check that we have the correct number of input arguments. 
    narginchk(2, inf);
    
   % parse the variable inputs
    pnames = {'ShortNameSuffix', 'MapToBinaryRelevance' 'NotAssessed' 'FixNumberRetrievedDocuments' 'Verbose'};
    dflts =  {[]                 'lenient'              'NotRelevant'         []                          false};
    
    if verLessThan('matlab', '9.2.0')
        [shortNameSuffix, mapToBinaryRelevance, notAssessed, fixNumberRetrievedDocuments, verbose, supplied] ...
         = matlab.internal.table.parseArgs(pnames, dflts, varargin{:});
    else
        [shortNameSuffix, mapToBinaryRelevance, notAssessed, fixNumberRetrievedDocuments, verbose, supplied] ...
         = matlab.internal.datatypes.parseArgs(pnames, dflts, varargin{:});
    end
    
    % actual parameters to be passed to assess.m, at least 6
    assessInput = cell(1, 10);
    
    % not assessed documents must be considered as not relevant for
    % precision
    assessInput{1, 1} = 'NotAssessed';
    assessInput{1, 2} = notAssessed;
    
    % map to binary relevance must be performed. Either use the value
    % passed by the caller or the default one
    assessInput{1, 3} = 'MapToBinaryRelevance';
    assessInput{1, 4} = mapToBinaryRelevance;
    
    % map to binary relevance weights to make follow-up computations
    % handier
    assessInput{1, 5} = 'MapToRelevanceWeights';
    assessInput{1, 6} = [0, 1];
     
    % padding is not needed
    assessInput{1, 7} = 'FixNumberRetrievedDocuments';
    assessInput{1, 8} = fixNumberRetrievedDocuments;
    
    % remove unsampled/unjudged documents because they are not appropriate
    % for average precision computation
    assessInput{1, 9} = 'RemoveUUFromPool';
    assessInput{1, 10} = true;
    
    if supplied.ShortNameSuffix
        if iscell(shortNameSuffix)
            % check that nameSuffix is a cell array of strings with one element
            assert(iscellstr(shortNameSuffix) && numel(shortNameSuffix) == 1, ...
                'MATTERS:IllegalArgument', 'Expected NameSuffix to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        shortNameSuffix = char(strtrim(shortNameSuffix));
        shortNameSuffix = shortNameSuffix(:).';
        
        % check that the nameSuffix is ok according to the matlab rules
        if ~isempty(regexp(shortNameSuffix, '\W*', 'once'))
            error('MATTERS:IllegalArgument', 'NameSuffix %s is not valid: it can contain only letters, numbers, and the underscore.', ...
                shortNameSuffix);
        end  
        
        % if it starts with an underscore, remove it since il will be
        % appended afterwards
        if strcmp(shortNameSuffix(1), '_')
            shortNameSuffix = shortNameSuffix(2:end);
        end
    end    
       
    if supplied.Verbose
        % check that verbose is a non-empty scalar logical value
        validateattributes(verbose, {'logical'}, {'nonempty','scalar'}, '', 'Verbose');
    end    
                     
    if verbose
        fprintf('\n\n----------\n');
        
        fprintf('Computing F-measure for run set %s with respect to pool %s: %d run(s) and %d topic(s) to be processed.\n\n', ...
            runSet.Properties.UserData.identifier, pool.Properties.UserData.identifier, width(runSet), height(runSet));
    end
    
    [assessedRunSet, poolStats, runSetStats, inputParams] = assess(pool, runSet, 'Verbose', verbose, assessInput{:});
    
     % the topic currently under processing
    ct = 1;
        
    % the run currently under processing
    cr = 1;
    
    % compute the measure topic-by-topic
    measuredRunSet = rowfun(@processTopic, assessedRunSet, 'OutputVariableNames', runSet.Properties.VariableNames, 'OutputFormat', 'table', 'ExtractCellContents', true, 'SeparateInputs', false);
    measuredRunSet.Properties.UserData.identifier = assessedRunSet.Properties.UserData.identifier;
    measuredRunSet.Properties.UserData.pool = pool.Properties.UserData.identifier;
    
    measuredRunSet.Properties.UserData.name = 'fMeasure';
    measuredRunSet.Properties.UserData.shortName = 'F';
    
    if strcmpi(notAssessed, 'condensed')
      measuredRunSet.Properties.UserData.name = 'condensedFmeasure_';
      measuredRunSet.Properties.UserData.shortName = 'condF_';
    end
    
    
    if ~isempty(shortNameSuffix)
        measuredRunSet.Properties.UserData.shortName = [measuredRunSet.Properties.UserData.shortName ...
            '_' shortNameSuffix];
    end

    if verbose
        fprintf('Computation of F-measure completed.\n');
    end
    
    %%
    
    % compute the measure for a given topic over all the runs
    function [varargout] = processTopic(topic)
        
        if(verbose)
            fprintf('Processing topic %s (%d out of %d)\n', pool.Properties.RowNames{ct}, ct, inputParams.topics);
            fprintf('  - run(s): ');
        end;
               
        % reset the index of the run under processing for each topic
        cr = 1;
         
        % compute the measure only on those column which contain the
        % actual runs
        varargout = cellfun(@processRun, topic);
              
        % increment the index of the current topic under processing
        ct = ct + 1;    
        
         if(verbose)
            fprintf('\n');
         end
        
        %% 
        
        % compute the measure for a given topic of a given run
        function [measure] = processRun(runTopic)
                              
            if(verbose)
                fprintf('%s ', runSet.Properties.VariableNames{cr});
            end;
            
            % avoid useless computations when you already know that either
            % the run has retrieved no relevant documents (0) or that there
            % are no relevant documents in the pool (NaN)
            if(runSetStats{ct, cr}.relevantRetrieved == 0)                
                if (poolStats{ct, 'NotRelevant'} == poolStats{ct, 'Assessment'})
                    measure = {NaN};
                else
                    measure = {0};
                end
                
                % increment the index of the current run under processing
                cr = cr + 1;
               
                return;
            end
                    
            % calculate precision
            P = runSetStats{ct, cr}.relevantRetrieved / runSetStats{ct, cr}.retrieved;
                                   
            % calculate recall
            R = runSetStats{ct, cr}.relevantRetrieved / poolStats{ct, 'RecallBase'};
            
            % calculate F-measure
            measure = 2 * P * R / (P + R);            
                                                               
            % properly wrap the results into a cell in order to fit it into
            % a value for a table
            measure = {measure};
            
            % increment the index of the current run under processing
            cr = cr + 1;
        end
    end
    
end



