%% compute_correlation
% 
% Computes correlation among measures for the given track and saves them 
% to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_correlation(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
%
% *Returns*
%
% Nothing
%

%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2018a or higher
% * *Copyright:* (C) 2018 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_correlation(trackID)
   
    % check the number of input arguments
    narginchk(1, 1);

    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
        
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing correlation among measures on track %s (%s) ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
        
    fprintf('+ Loading measures\n');
        
    measures = cell(1, EXPERIMENT.measure.number);
    
    topics = NaN;
    runs = NaN;
    
    % for each measure
    for m = 1:EXPERIMENT.measure.number
        
        mid = EXPERIMENT.measure.list{m};
        
        fprintf('  - loading %s\n', mid);
        
        measureID = EXPERIMENT.pattern.identifier.measure(mid, trackID);
        
        serload2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
            'WorkspaceVarNames', {'measure'}, ...
            'FileVarNames', {measureID});
        
        measures{m} = measure;
        
        if ~isnan(topics)
            % check that all the measures have the same number of topics
            % and runs
            assert(topics == height(measure), 'All the measures must have the same number of topics. %s has %d instead of %d topics.', ...
                mid, height(measure), topics);
            
            assert(runs == width(measure), 'All the measures must have the same number of runs. %s has %d instead of %d runs.', ...
                mid, width(measure), runs);
        else            
            topics = height(measure);
            runs = width(measure);
        end
        
        clear('measure');        
    end
    
    fprintf('\n+ Preparing data to be processed\n\n');
    
    % each column is the average of a measure across the topics for a given
    % measures
    data.overall = NaN(runs, EXPERIMENT.measure.number);
    
    % each plane is a topic; each column of a matrix is a measure, each row
    % of a matrix is the score of a run (in that topic for that measure)
    data.topics = NaN(runs, EXPERIMENT.measure.number, topics);
    
    
    % for each measure
    for m = 1:EXPERIMENT.measure.number
        
       % compute the average of the measure across the topics and save it
       % as a column in data
       data.overall(:, m) = mean(measures{m}{:, :}).'; 
       
       for t = 1:topics
          
           % extract the values (for all the runs) of a measure for the
           % given topic and save them as a column in the correct plane of
           % data
           data.topics(:, m, t) = measures{m}{t, :}.';
           
       end
    end
    
    %clear('measures');
    
    fprintf('+ Computing correlation\n\n');
    
    % indexes of the upper triangle of the correlation matrix
    idx = logical(tril(ones(EXPERIMENT.measure.number), -1));
    
    correlation.id = EXPERIMENT.analysis.correlation.id;
    
    correlation.labels = EXPERIMENT.analysis.correlation.labels.list;
    
    % the correlation over the topic averages (standard way to compute it)
    correlation.overall.values = EXPERIMENT.analysis.correlation.compute(data.overall);
    correlation.overall.values = correlation.overall.values(idx).';
    
    % the correlation values topic-by-topic
    correlation.topics.values = NaN(topics, EXPERIMENT.analysis.correlation.labels.number);
    correlation.topics.mean = NaN;
    correlation.topics.std = NaN;
        
    for t = 1:topics
        tmp = EXPERIMENT.analysis.correlation.compute(data.topics(:, :, t));
        correlation.topics.values(t, :) = tmp(idx).';
    end
    
    correlation.topics.mean = mean(correlation.topics.values);
    correlation.topics.std = std(correlation.topics.values);
    
    corrID = EXPERIMENT.pattern.identifier.correlation(EXPERIMENT.analysis.correlation.id, trackID);
    
    sersave2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
        'WorkspaceVarNames', {'correlation'}, ...
        'FileVarNames', {corrID});
           
    fprintf('\n\n######## Total elapsed time for computing correlation on track %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
